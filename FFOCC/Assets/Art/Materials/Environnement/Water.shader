Shader "Unlit/Water" {
    Properties {
        _MainColor ("Water Tint", Color) = (110, 140, 222, 235)
        _WaveAmplitude ("Wave Amplitude", Range(0, 10)) = 1
        _WaveFrequency ("Wave Frequency", Range(0, 10)) = .5
        _WaveSpeed ("Wave Speed", Range(0, 10)) = 1
        _RandomFactorX ("Random Factor X", Range(0, 1)) = .314159
        _RandomFactorZ ("Random Factor Z", Range(0, 1)) = .756535
    }
    SubShader {
        Tags { "RenderType"="Transparent" "Queue"="Transparent" }
        LOD 100

        Pass {
            Cull Off // Disable backface culling
            Blend SrcAlpha OneMinusSrcAlpha // Enable transparency

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            fixed4 _MainColor;
            float _WaveAmplitude;
            float _WaveFrequency;
            float _WaveSpeed;
            float _RandomFactorX;
            float _RandomFactorZ;

            struct appdata {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float3 normal : NORMAL; // Add this to get the normal vector of the vertex
            };

            struct v2f {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                UNITY_FOG_COORDS(1)
                float3 normal : TEXCOORD2; // Add this to pass the normal vector to the fragment shader
                
            };

            v2f vert (appdata v) {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.vertex.y += _WaveAmplitude * sin(_Time.y * _WaveSpeed + (v.vertex.x + _RandomFactorX) * _WaveFrequency);
                o.vertex.y += _WaveAmplitude * sin(_Time.y * _WaveSpeed + (v.vertex.z + _RandomFactorZ) * _WaveFrequency);
                o.normal = UnityObjectToWorldNormal(v.normal); // Transform the normal vector to world space
                o.uv = v.uv;
                UNITY_TRANSFER_FOG(o, o.vertex);
                return o;
            }

            fixed4 frag (v2f i) : SV_Target {
                fixed3 worldNormal = normalize(i.normal); // Normalize the normal vector
                fixed3 worldLightDir = normalize(_WorldSpaceLightPos0.xyz); // Get the direction of the light
                fixed4 color = _MainColor * max(0, dot(worldNormal, worldLightDir)); // Calculate the lighting
                color.a = _MainColor.a;
                UNITY_APPLY_FOG(i.fogCoord, color);
                return color;
            }

            ENDCG
        }
    }
}