﻿// Alan Zucconi
// www.alanzucconi.com
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



[ExecuteInEditMode]
public class ColorBlindFilter : MonoBehaviour
{
    [Range(0f, 1f)]
    public float RedIntensity = 1f;
    [Range(0f, 1f)]
    public float BlueIntensity = 1f;
    [Range(0f, 1f)]
    public float GreenIntensity = 1f;

    private Material material;
    private Graphic[] graphics;

    private Color[] originalColor;

    void Awake()
    {
        material = new Material(Shader.Find("Hidden/ChannelMixer"));
        material.SetColor("_R", new Color(RedIntensity, 0, 0));
        material.SetColor("_G", new Color(0, BlueIntensity, 0));
        material.SetColor("_B", new Color(0, 0, GreenIntensity));

        graphics = FindObjectsOfType<Graphic>();


        List<Color> colors = new List<Color>();
        foreach (Graphic g in graphics)
        {
            colors.Add(g.color);
        }
        originalColor = colors.ToArray();
    }


    void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        material.SetColor("_R", new Color(RedIntensity, 0, 0));
        material.SetColor("_G", new Color(0, BlueIntensity, 0));
        material.SetColor("_B", new Color(0, 0, GreenIntensity));

        Graphics.Blit(source, destination, material, 0);
    }

    void Update()
    {
        for (int i = 0; i < graphics.Length; i++)
        {
            graphics[i].color = new Color(
                               originalColor[i].r * RedIntensity,
                                              originalColor[i].g * GreenIntensity,
                                                             originalColor[i].b * BlueIntensity,
                                                                            originalColor[i].a
                                                                                       );
        }
    }
}
