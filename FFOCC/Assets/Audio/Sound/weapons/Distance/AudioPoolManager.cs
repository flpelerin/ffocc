using UnityEngine;
using System.Collections.Generic;

public class AudioPoolManager : MonoBehaviour
{
    public List<AudioSource> poolSons = new List<AudioSource>();
    private int currentIndex = 0;

    public int nbsound;
    [Range(0f, 1f)]
    public float volume; // Volume initial (par d�faut)

    public static AudioPoolManager instance;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        // Cr�ez un pool initial d'instances AudioSource
        for (int i = 0; i < nbsound; i++) // Ajustez la taille du pool selon vos besoins
        {
            AudioSource nouveauSon = gameObject.AddComponent<AudioSource>();
            poolSons.Add(nouveauSon);
        }
    }

    // Fonction pour d�clencher un nouveau son
    public void PlaySound(AudioClip clip)
    {
        // Obtenez l'instance AudioSource du pool
        AudioSource sonLibre = poolSons[currentIndex];

        // Incr�mentez l'index pour la prochaine fois
        currentIndex = (currentIndex + 1) % poolSons.Count;

        // Arr�tez le son actuel et jouez le nouveau son
        sonLibre.Stop();
        sonLibre.clip = clip;
        sonLibre.volume = volume;
        sonLibre.Play();
    }
}
