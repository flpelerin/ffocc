Shader "Hidden/Vignette"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"


            sampler2D _MainTex;
            float _Radius, _Feather;

            float4 _TintColor;

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };



            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 color = tex2D(_MainTex, i.uv);

                float2 newUV = i.uv * 2 - 1;
                float dist = length(newUV);
                float mask = 1 - smoothstep(_Radius, _Radius + _Feather, dist);
                float invertMask = 1 - mask;

                float3 displayColor = color.rgb * mask;
                float3 vignetteColor = color.rgb * invertMask * _TintColor.rgb;

                return fixed4(displayColor + vignetteColor, 1);
            }
            ENDCG
        }
    }
}
