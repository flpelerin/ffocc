using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
using TMPro;

public class SettingsMenu : MonoBehaviour
{
    public float currentVolume;
    public TMP_Dropdown resolutionDropdown;
    public Toggle fullscreenToggle;
    public Slider volumeSlider;
    public AudioMixer audioMixer;
    
    Resolution[] resolutions;
    public int currentResolutionIndex;

    public SettingsSave settingsSave;

    public GameObject settingsWindow;
    public GameObject customInputWindow;
    public GameObject customShadersWindow;

    public ButtonAnimation settingsButtonAnimation;
    

    private static SettingsMenu instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        resolutions = Screen.resolutions;
        resolutionDropdown.ClearOptions();

        List<Resolution> uniqueRes = new List<Resolution>();

        List<string> options = new List<string>();

        for (int i = 0; i < resolutions.Length; i++)
        {
            string option = resolutions[i].width + "x" + resolutions[i].height;

            // Check if the option already exists in the list
            if (!options.Contains(option))
            {
                uniqueRes.Add(resolutions[i]);
                options.Add(option);
            }
        }

        for (int i = 0; i < uniqueRes.Count; i++)
        {
            if (uniqueRes[i].width == Screen.currentResolution.width && uniqueRes[i].height == Screen.currentResolution.height)
            {
                currentResolutionIndex = i;
            }
        }

        resolutions = uniqueRes.ToArray();

        resolutionDropdown.AddOptions(options);
        resolutionDropdown.value = currentResolutionIndex;
        resolutionDropdown.RefreshShownValue();
    }

    private void Start() {

        if (settingsSave != null)
        {
            settingsSave.LoadSettings(); // Charge les paramètres sauvegardés au démarrage
        }
        customShadersWindow.SetActive(false);
        settingsWindow.SetActive(false);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && settingsWindow.activeSelf && !customInputWindow.activeSelf)
        {
            CloseSettingsButton();
        }
    }

    private void FixedUpdate() {
        audioMixer.GetFloat("volume", out currentVolume);
    }
    public void SetVolume(float volume) // Fonction déclenchée lors de la modification du volume
    {
        audioMixer.SetFloat("volume", volume);
    }

    public void SetVolumeSlider()
    {
        audioMixer.GetFloat("volume", out currentVolume);
        volumeSlider.value = currentVolume;
    }

    public void SetFullScreen(bool isFullScreen) // Fonction déclenchée lors de la modification du parametre fullscreen
    {
        Screen.fullScreen = isFullScreen;
    }

    public void SetResolution(int resolutionIndex) // Fonction déclenchée lors de la modification de la resolution
    {
        Resolution resolution = resolutions[resolutionIndex];
        Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);
        resolutionDropdown.value = resolutionIndex;
        resolutionDropdown.RefreshShownValue();
    }

    public void CustomInputButton()
    {
        customInputWindow.SetActive(true);
    }

    public void CustomShadersButton()
    {
        customShadersWindow.SetActive(true);
    }

    public void CloseSettingsButton()
    {
        settingsButtonAnimation.ReinitializeText();
        settingsWindow.SetActive(false);
    }

}