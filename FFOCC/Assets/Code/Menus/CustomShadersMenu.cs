using UnityEngine;
using UnityEngine.UI;

public class CustomShadersMenu : MonoBehaviour
{
    public GameObject customShadersWindow;
    public ButtonAnimation customShadersButtonAnimation;
    public Slider redSlider;
    public Slider greenSlider;
    public Slider blueSlider;
    private ColorBlindFilter colorBlindFilter;

    private void Start()
    {
        colorBlindFilter = Camera.main.GetComponent<ColorBlindFilter>();

        // Retrieve values from PlayerPrefs and assign them to sliders and colorBlindFilter
        float redIntensity = PlayerPrefs.GetFloat("RedIntensity");
        float greenIntensity = PlayerPrefs.GetFloat("GreenIntensity");
        float blueIntensity = PlayerPrefs.GetFloat("BlueIntensity");

        redSlider.value = redIntensity;
        greenSlider.value = greenIntensity;
        blueSlider.value = blueIntensity;

        colorBlindFilter.RedIntensity = redIntensity;
        colorBlindFilter.GreenIntensity = greenIntensity;
        colorBlindFilter.BlueIntensity = blueIntensity;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && customShadersWindow.activeSelf)
        {
            CloseCustomShadersButton();
        }
    }

    public void SetRed(float value)
    {
        colorBlindFilter.RedIntensity = value;
        PlayerPrefs.SetFloat("RedIntensity", value);
    }

    public void SetGreen(float value)
    {
        colorBlindFilter.GreenIntensity = value;
        PlayerPrefs.SetFloat("GreenIntensity", value);
    }

    public void SetBlue(float value)
    {
        colorBlindFilter.BlueIntensity = value;
        PlayerPrefs.SetFloat("BlueIntensity", value);
    }

    public void CloseCustomShadersButton()
    {
        customShadersButtonAnimation.ReinitializeText();
        customShadersWindow.SetActive(false);
    }
}
