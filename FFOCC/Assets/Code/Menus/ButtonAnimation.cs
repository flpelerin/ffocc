using UnityEngine;
using TMPro;
using UnityEngine.EventSystems;

public class ButtonAnimation : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] private TextMeshProUGUI buttonText;
    private string originalText;
    private string selectionChar = "> ";

    void Start()
    {
        // Assurez-vous que le GameObject du bouton a un composant TextMeshProUGUI
        buttonText = GetComponentInChildren<TextMeshProUGUI>();

        // Enregistrez le texte initial au démarrage
        originalText = buttonText.text;
    }

    void Update()
    {
        // Vérifiez si la touche "Échap" est enfoncée et que la souris est toujours sur le bouton
        if (Input.GetKey(KeyCode.Escape))
        {
            // Réinitialisez le texte du bouton à sa version originale
            buttonText.text = originalText;
        }
    }

    public void ReinitializeText()
    {
        buttonText.text = originalText;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        // Ajouter la flèche '>' au texte du bouton
        buttonText.text = selectionChar + originalText;
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        // Réinitialisez le texte du bouton à sa version originale
        ReinitializeText();
    }
}