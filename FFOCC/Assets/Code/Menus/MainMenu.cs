using UnityEngine;
using UnityEngine.SceneManagement;


public class MainMenu : MonoBehaviour
{
    public GameObject settingsWindow;

    void Update()
    {
        
    }

    public void NewGameButton(string scenename)
    {
        SceneManager.LoadScene(scenename);
    }

    public void SettingsButton()
    {
        settingsWindow.SetActive(true);
    }

    public void ExitButton()
    {
        Application.Quit();
    }
}
