using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomInputMenu : MonoBehaviour
{
    public GameObject customInputWindow;
    public ButtonAnimation customInputButtonAnimation;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && customInputWindow.activeSelf)
        {
            CloseCustomInputButton();
        }
    }

    public void CloseCustomInputButton()
    {
        customInputButtonAnimation.ReinitializeText();
        customInputWindow.SetActive(false);
    }
    
}
