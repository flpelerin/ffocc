using UnityEngine;
using UnityEngine.Audio;

public class SettingsSave : MonoBehaviour
{
    public float savedVolume = 0f;
    public bool savedFullScreen = true;
    public int savedResolutionIndex = 0;
    public AudioMixer audioMixer;
    public SettingsMenu settingsMenu;


    public void SaveSettings()
    {
        // Sauvegarde volume
        savedVolume = settingsMenu.currentVolume; // Met à jour la valeur sauvegardée
        PlayerPrefs.SetFloat("volume", savedVolume);

        // Sauvegarde état plein écran
        savedFullScreen = settingsMenu.fullscreenToggle.isOn;

        if (savedFullScreen)
            PlayerPrefs.SetInt("fullscreen", 1); // 1 pour true, 0 pour false
        else
            PlayerPrefs.SetInt("fullscreen", 0); // 1 pour true, 0 pour false

        // Sauvegarde index de la résolution
        savedResolutionIndex = settingsMenu.resolutionDropdown.value;
        PlayerPrefs.SetInt("resolution", savedResolutionIndex);

        PlayerPrefs.Save();
        //Debug.Log("Sauvé");
    }

    public void LoadSettings()
    {
        // Trouver l'objet contenant le script SettingsMenu dans la scène
        settingsMenu = FindObjectOfType<SettingsMenu>();

        if (PlayerPrefs.HasKey("volume"))
        {
            // Chargement volume
            savedVolume = PlayerPrefs.GetFloat("volume"); // Applique la valeur chargée à l'AudioMixer
            audioMixer.SetFloat("volume", savedVolume);
            settingsMenu.SetVolumeSlider();
        }

        if (PlayerPrefs.HasKey("fullscreen"))
        {
            // Chargement du mode plein écran
            savedFullScreen = (PlayerPrefs.GetInt("fullscreen") == 1);
            settingsMenu.fullscreenToggle.isOn = savedFullScreen;
        }

        if (PlayerPrefs.HasKey("resolution"))
        {
            // Chargement du mode plein écran
            savedResolutionIndex = PlayerPrefs.GetInt("resolution");
            settingsMenu.SetResolution(savedResolutionIndex);
        }
        //Debug.Log("Chargé");
    }

    

}