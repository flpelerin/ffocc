using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuManager : MonoBehaviour
{
    public PauseMenu pauseMenu;
    public SettingsMenu settingsMenu;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if(PauseMenu.gameIsPaused)
            {
                pauseMenu.Resume();
            }
            else
            {
                pauseMenu.Paused();
            }
        }

    }
}
