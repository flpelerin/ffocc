using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class PlayerSwim : MonoBehaviour
{
    public GameObject ocean;
    public FogManagement fog;
    bool IsUnderWater =false;
    private void LateUpdate()
    {
        if (this.gameObject.transform.position.y < ocean.gameObject.transform.position.y+ 0.2f && IsUnderWater == false)
        {
            IsUnderWater = !IsUnderWater;
            fog.PlayerUnderDaWater(IsUnderWater);
            ocean.GetComponent<Renderer>().enabled = false;
        }
        else
        {
            if(this.gameObject.transform.position.y > ocean.gameObject.transform.position.y+0.2f && IsUnderWater == true)
            {
                IsUnderWater = !IsUnderWater;
                fog.PlayerUnderDaWater(IsUnderWater);
                ocean.GetComponent<Renderer>().enabled = true;
            }
        }
    }
}
