using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireCamp : MonoBehaviour
{
    public bool IsLight;
    public float damage;                // Valeur de dommage � infliger au joueur
    public PlayerStats player;
    public float damageDistance = 5f;   // Distance � laquelle les d�g�ts sont inflig�s

    private void Awake()
    {
        player = PlayerStats.FindObjectOfType<PlayerStats>();
    }

    // Update is called once per frame
    void Update()
    {
        // V�rifie si le joueur est � une distance sp�cifi�e du centre du feu
        if (IsLight && Vector3.Distance(player.transform.position, gameObject.transform.position) < damageDistance)
        {
            // Inflige des d�g�ts au joueur chaque seconde si il est dans le feu
            player.TakeDamage(Time.deltaTime * damage);
        }
    }
}
