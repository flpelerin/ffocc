using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageCollision : MonoBehaviour
{
    ZombieAI Zombie;
    private void Awake()
    {
        Zombie = this.GetComponentInParent<ZombieAI>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (Zombie.CanHit())
        {
            if (other.gameObject.CompareTag("Player"))
            {
                Zombie.HitPlayer();
            }
        }
    }
}
