using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class DaytimeSystem : MonoBehaviour
{
    public static float daytime = 0f;

    public float updateDelay = 1f;
    public float dayDuration = 720;    // in seconds
    public GameObject worldLight;

    private Vector3 baseWorldLightRotation;

    private float baseWorldLightIntensity,
                  baseTime;


    GameObject GetWorldLight()
    {
        Light[] lights = GameObject.FindObjectsOfType<Light>();

        foreach (Light light in lights)
            if (light.type == LightType.Directional)
                return light.gameObject;

        return null;
    }

    float clamp(float x, float min, float max) { return x < min ? min : x > max ? max : x; }

    float SunIntensity()
    {
        float x = daytime > .5f ? 0 : daytime * 2f;
        x = (.5f - Mathf.Abs(x - .5f)) * 4;
        return clamp(x, 0, 1);
    }


    IEnumerator UpdateDaytime()
    {
        daytime = (((TimeSystem.gameTime/ dayDuration) + baseTime) % 1f);
        worldLight.transform.rotation = Quaternion.Euler(new Vector3(daytime * 360f, baseWorldLightRotation.y, baseWorldLightRotation.z));

        RenderSettings.skybox.SetColor("_SkyGradientTop", AtmosphereSystem.atmosphereColor);
        RenderSettings.skybox.SetColor("_SkyGradientBottom", AtmosphereSystem.atmosphereColor);

        worldLight.GetComponent<Light>().intensity = baseWorldLightIntensity * SunIntensity();

        yield return new WaitForSeconds(updateDelay);
        StartCoroutine(UpdateDaytime());
    }

    void Awake()
    {
        worldLight = worldLight == null ? GetWorldLight() : worldLight;
        baseTime = (worldLight.transform.rotation.eulerAngles.x / 360);

        baseWorldLightIntensity = worldLight.GetComponent<Light>().intensity;

        StartCoroutine(UpdateDaytime());
    }
}