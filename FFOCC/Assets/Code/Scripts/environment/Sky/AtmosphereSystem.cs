using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public struct athmosphere_t {
    public Color day,
                 night;
};

public class AtmosphereSystem : MonoBehaviour
{
    public athmosphere_t athmosphere;
    public static Color atmosphereColor;

    public float updateDelay = 1f;


    float Clip(float x, float min, float max) { return x < min ? min : x > max ? max : x; }

    float SunIntensity() {
        float x = DaytimeSystem.daytime > .5f ? 0 : DaytimeSystem.daytime * 2f;
        x = (.5f - Mathf.Abs(x - .5f)) * 4;
        return Clip(x, 0, 1);
    }

    public Color GetColor() { return Color.Lerp(athmosphere.night, athmosphere.day, SunIntensity()); }


    IEnumerator UpdateAtmosphere() {
        atmosphereColor = GetColor();

        yield return new WaitForSeconds(updateDelay);
        StartCoroutine(UpdateAtmosphere());
    }

    void Awake() {
        StartCoroutine(UpdateAtmosphere());
    }
}
