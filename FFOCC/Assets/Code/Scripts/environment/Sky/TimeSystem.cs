using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class TimeSystem : MonoBehaviour
{

    /* global time variables, accessible from anywhere within the project */
    
    public static float gameTime = 0f;  // never resets
    
    public float updateDelay = 0.1f;



    void Awake() {
        StartCoroutine(UpdateTimeSystem());
    }


    IEnumerator UpdateTimeSystem() {
        gameTime += updateDelay;

        yield return new WaitForSeconds(updateDelay);
        StartCoroutine(UpdateTimeSystem());
    }

}
