using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public struct fog_density_t {
    public float max,
                 min;
};


public class FogSystem : MonoBehaviour
{
    public float updateDelay = 10f;
    public fog_density_t fogDensity;


    float FogDensity() {
        float x = DaytimeSystem.daytime > .5f ? 0 : DaytimeSystem.daytime * 2f;
        x = 1 - ((.5f - Mathf.Abs(x - .5f)) * 2);
        return x * (fogDensity.max - fogDensity.min) + fogDensity.min;
    }


    IEnumerator UpdateFog () {
        RenderSettings.fogColor = AtmosphereSystem.atmosphereColor;
        RenderSettings.fogDensity = FogDensity();

        yield return new WaitForSeconds(updateDelay);
        StartCoroutine(UpdateFog());
    }


    void Awake () {
        if (RenderSettings.fog == true && RenderSettings.fogDensity != 0) {
            if (fogDensity.max == 0)
                fogDensity.max = RenderSettings.fogDensity;
            if (fogDensity.min == 0)
                fogDensity.min = fogDensity.max / 10f;
        }

        RenderSettings.fog = true;
        StartCoroutine(UpdateFog());
    }
}
