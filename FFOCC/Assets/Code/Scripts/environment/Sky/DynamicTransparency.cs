using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DynamicTransparency : MonoBehaviour
{
    public float updateDelay = 1f;
    private Material selfMaterial;

    private Color baseColor;


    float clamp(float x, float min, float max) { return x < min ? min : x > max ? max : x; }

    float SunIntensity() {
        float x = DaytimeSystem.daytime > .5f ? 0 : DaytimeSystem.daytime * 2f;
        x = (.5f - Mathf.Abs(x - .5f)) * 6;
        return clamp(x, 0, 1);
    }

    IEnumerator UpdateStars() {
        selfMaterial.SetColor("_MainColor", new Color(baseColor.r, baseColor.g, baseColor.b, baseColor.a - SunIntensity()));

        yield return new WaitForSeconds(updateDelay);
        StartCoroutine(UpdateStars());
    }

    void Awake() {
        selfMaterial = GetComponent<Renderer>().material;
        baseColor = selfMaterial.GetColor("_MainColor");

        StartCoroutine(UpdateStars());
    }
}
