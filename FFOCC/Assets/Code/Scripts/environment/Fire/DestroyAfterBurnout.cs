using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAfterBurnout : MonoBehaviour
{
    
    public float health = 20f;

    private TemperatureReceiver receiver;
    private FlamableSystem flamableSystem;


    IEnumerator UpdateHealth() {
        if (receiver.temperature > flamableSystem.flamingThreshold)
            health -= receiver.updateDelay;

        if (health <= 0 && transform.parent.parent.gameObject)
            Destroy(transform.parent.parent.gameObject);

        yield return new WaitForSeconds(receiver.updateDelay);                
        StartCoroutine(UpdateHealth());

    }


    void Awake() {
        receiver = transform.parent.GetComponent<TemperatureReceiver>();
        flamableSystem = GetComponent<FlamableSystem>();

        StartCoroutine(UpdateHealth());
    }
}
