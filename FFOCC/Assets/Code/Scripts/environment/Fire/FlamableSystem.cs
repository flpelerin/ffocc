using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class FlamableSystem : MonoBehaviour
{
    public float flamingThreshold;
    public GameObject flameObject;

    private TemperatureReceiver receiver;

    private GameObject flameInstance;


    IEnumerator UpdateFireSystem() {
        if (receiver.temperature > flamingThreshold)
             flameInstance.SetActive(true);
        else flameInstance.SetActive(false);

        yield return new WaitForSeconds(receiver.updateDelay);
        StartCoroutine(UpdateFireSystem());
    }


    void Awake() {
        receiver = transform.parent.GetComponent<TemperatureReceiver>();
        
        if (flamingThreshold == 0)
            flamingThreshold = receiver.temperatureRange.max * 2;

        flameInstance = Instantiate(flameObject, transform.position, flameObject.transform.rotation, transform);
        flameInstance.transform.localScale = new Vector3(.2f, .2f, .2f);
        flameInstance.name = flameObject.name + "_instance";
        
        TemperatureEmitter flameEmitter = flameInstance.GetComponentInChildren<TemperatureEmitter>();
        if (flameEmitter.temperature < flamingThreshold * 2)
            flameEmitter.temperature = flamingThreshold * 2;

        StartCoroutine(UpdateFireSystem());
    }
}
