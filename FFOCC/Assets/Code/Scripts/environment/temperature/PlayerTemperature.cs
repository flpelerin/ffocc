using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public enum effect_type_t {
    none,
    cold,
    hot
};


public struct effect_t {
    public float intensity;
    public effect_type_t type;
};


[System.Serializable]
public struct temp_vignette_t {
    public Color hot,
                 cold;
};

[System.Serializable]
public struct temp_particle_t {
    public GameObject hot,
                      cold;
    public Vector3 offset;

 
};

public class PlayerTemperature : MonoBehaviour
{
    public temp_vignette_t tempVignette = new temp_vignette_t { hot = new Color(1f, 0.7568628f, 0.372549f, 1f), cold = new Color(0.372549f, 0.5314569f, 1f, 1f) };
    public temp_particle_t tempParticles = new temp_particle_t { hot = null, cold = null, offset = new Vector3(0f, -.25f, .5f) };
    
    public float temperatureDamage = 1f;

    public TemperatureReceiver receiver;
    public GameObject camera;
    public temp_range_t temperatureRange;
    public float updateDelay;
    
    public VignetteEffect vignetteEffect;

    // emission module of particle system
    private ParticleSystem.EmissionModule particleInstHot,
                                          particleInstCold;

    public PlayerStats playerStats;


    float Clip(float x, float min, float max) { return x < min ? min : x > max ? max : x; }

    IEnumerator UpdateTemperature() {
        if (receiver.temperature < temperatureRange.min || receiver.temperature > temperatureRange.max)
            gameObject.SendMessage("TakeDamage", (updateDelay * temperatureDamage));


        effect_t vignette = ComputeEffect(temperatureRange.max / 4f, temperatureRange.max / 1.33f, temperatureRange.max / 2f);

        switch (vignette.type) {
            case effect_type_t.cold:
                vignetteEffect.tintColor = tempVignette.cold;
                vignetteEffect.radius = 2 - Clip(vignette.intensity * 2, 0f, 1.8f);
                break;

            case effect_type_t.hot:
                vignetteEffect.tintColor = tempVignette.hot;
                vignetteEffect.radius = 2 - Clip(vignette.intensity * 2, 0f, 1.8f);
                break;

            case effect_type_t.none:
                vignetteEffect.tintColor = Color.black;
                vignetteEffect.radius = 2;
                break;
        }

        effect_t particles = ComputeEffect(temperatureRange.max / 3f, temperatureRange.max / 1.66f, temperatureRange.max / 2f);

        switch (particles.type) {
            case effect_type_t.cold:
                particleInstHot.enabled = false;
                particleInstCold.enabled = true;
                break;

            case effect_type_t.hot:
                particleInstHot.enabled = true;
                particleInstCold.enabled = false;
                break;

            case effect_type_t.none:
                particleInstHot.enabled = false;
                particleInstCold.enabled = false;
                break;
        }

        playerStats.temperature = receiver.temperature;

        yield return new WaitForSeconds(updateDelay);
        StartCoroutine(UpdateTemperature());        
    }


    effect_t ComputeEffect(float threshold_cold, float threshold_hot, float medium) {
        float temperature = receiver.temperature;

        float hot = threshold_hot - temperature,
              cold = temperature - threshold_cold;

        if (hot < 0)       return new effect_t { intensity = hot / (medium - threshold_hot), type = effect_type_t.hot };
        else if (cold < 0)   return new effect_t { intensity = cold / (threshold_cold - medium), type = effect_type_t.cold };
        else                return new effect_t { intensity = 0f, type = effect_type_t.none };
    }   

    void Awake() {
        /*receiver = GetComponentInChildren<TemperatureReceiver>();
        vignetteEffect = GetComponentInChildren<VignetteEffect>();
        playerStats = GetComponent<PlayerStats>();
        camera = GetComponentInChildren<VignetteEffect>().GameObject;
        updateDelay = receiver.GameObject.GetComponent<SphereAreaTrigger>().updateDelay;
        temperatureRange = receiver.temperatureRange;
*/
        
        particleInstHot = Instantiate(tempParticles.hot, camera.transform.position + tempParticles.offset, Quaternion.identity, camera.transform).GetComponent<ParticleSystem>().emission;      
        particleInstCold = Instantiate(tempParticles.cold, camera.transform.position + tempParticles.offset, Quaternion.identity, camera.transform).GetComponent<ParticleSystem>().emission;      


        if (receiver == null)
            Debug.LogError("PlayerTemperature: TemperatureReceiver not found");

        StartCoroutine(UpdateTemperature());
    }

}
