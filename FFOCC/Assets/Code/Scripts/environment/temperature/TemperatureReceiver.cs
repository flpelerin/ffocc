using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public struct temp_range_t {
    public float min, max;
};

public class TemperatureReceiver : MonoBehaviour
{
    public temp_range_t temperatureRange = new temp_range_t { min = 0f, max = 40f };

    public float temperature,     // temperature in decrees
                 warmthRating;     // clothes & stuff

    public float updateDelay = 0.1f;

    private SphereAreaTrigger sphereTrigger;
    private DrawSphereGizmo gizmo;

    
    private List<GameObject> temperatureSources = new List<GameObject>();


    float Clip(float x, float min, float max) { return x < min ? min : x > max ? max : x; }

    void UpdateTemperatureGizmo() {
        float f = Clip(((temperatureRange.max / 2) + temperature) / (temperatureRange.max * 2), 0, 1);
        
        if (gizmo != null)
            gizmo.color = new Color(f, 0, 1 - f);
    }

    void CoolDown() {
        if (temperatureSources.Count == 0)
            temperature += Clip((((temperatureRange.max / 2f) - temperature) / 10f) * updateDelay, -10, 10);
    }


    void tempSourceCleanup() {
        for (int i = 0; i < temperatureSources.Count; ) {
            if (temperatureSources[i] == null) {
                temperatureSources.RemoveAt(i);
                continue;
            }
            ++i;
        }
    }


    IEnumerator UpdateTemperature() {
        tempSourceCleanup();
        foreach (GameObject source in temperatureSources) {
            
            TemperatureEmitter emitter = source.GetComponent<TemperatureEmitter>();
            if (emitter == null) continue;

            float distance = (Vector3.Distance(transform.position, source.transform.position) / sphereTrigger.GetRadius());
            float diff = (emitter.temperature - temperature) / (warmthRating > 0 ? warmthRating : 1);
            
            temperature += Clip((diff / (emitter.useProximity ? distance / 1.5f : 1)) * updateDelay, -10, 10);
        }


        UpdateTemperatureGizmo();
        CoolDown();

        yield return new WaitForSeconds(updateDelay);
        StartCoroutine(UpdateTemperature());
    }


    public void TemperatureAreaEnter(GameObject obj) { 
        if (obj.transform.IsChildOf(transform) || temperatureSources.Contains(obj))
            return;

        temperatureSources.Add(obj); 
    }
    public void TemperatureAreaExit(GameObject obj) { temperatureSources.Remove(obj); }



    public GameObject GameObject { get { return gameObject; } }


    void Start() {
        sphereTrigger = GetComponent<SphereAreaTrigger>();
        gizmo = GetComponent<DrawSphereGizmo>();

        temperature = temperatureRange.max / 2;
        if (warmthRating == 0)
            warmthRating = temperature;

        StartCoroutine(UpdateTemperature());
    }
}
