using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class TemperatureEmitter : MonoBehaviour
{
    public temp_range_t temperatureRange = new temp_range_t { min = 0f, max = 40f };

    public float temperature = 20f;      // temperature in decrees
    public bool useProximity = false;    // use proximity to calculate temperature

    private DrawSphereGizmo gizmo;


    float Clip(float x, float min, float max) { return x < min ? min : x > max ? max : x; }

    void UpdateTemperatureGizmo() {
        float f = Clip(((temperatureRange.max / 2) + temperature) / (temperatureRange.max * 2), 0, 1);
        
        if (gizmo != null)
            gizmo.color = new Color(f, 0, 1 - f);
    }

    void OnDrawGizmos() {
        if (gizmo != null)
            UpdateTemperatureGizmo();
    }


    void OnSphereAreaEnter(GameObject obj) {
        if (obj == null)
            return;

        TemperatureReceiver receiver = obj.GetComponent<TemperatureReceiver>();
        if (receiver == null)
            return;

        obj.SendMessage("TemperatureAreaEnter", gameObject);
    }

    void OnSphereAreaExit(GameObject obj) {
        if (obj == null)
            return;

        TemperatureReceiver receiver = obj.GetComponent<TemperatureReceiver>();
        if (receiver == null)
            return;
        
        obj.SendMessage("TemperatureAreaExit", gameObject);
    }

    void Awake() {
        gizmo = GetComponent<DrawSphereGizmo>();
        
        if (gizmo != null)
            UpdateTemperatureGizmo();
    }
}
