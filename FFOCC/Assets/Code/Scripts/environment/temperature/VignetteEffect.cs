using System.Collections;
using System.Collections.Generic;
using UnityEngine;



[ExecuteInEditMode]
public class VignetteEffect : MonoBehaviour
{
    // Applies post processing effect "Vignette.shader" to camera (self)

    public Shader vignetteShader;

    public float radius,
                 feather;

    public Color tintColor = Color.black;

    private Material vignetteMaterial;


    public GameObject GameObject { get { return gameObject; } }


    void Awake () {
        vignetteMaterial = new Material(vignetteShader);
    }


    void OnRenderImage (RenderTexture src, RenderTexture dest) {
        RenderTexture startRenderTexture = RenderTexture.GetTemporary(src.width, src.height);

        vignetteMaterial.SetFloat("_Radius", radius);
        vignetteMaterial.SetFloat("_Feather", feather);
        vignetteMaterial.SetColor("_TintColor", tintColor);

        Graphics.Blit(src, startRenderTexture, vignetteMaterial);
        Graphics.Blit(startRenderTexture, dest);
        RenderTexture.ReleaseTemporary(startRenderTexture);
    }
}
