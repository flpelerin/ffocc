using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DaytimeTemperature : MonoBehaviour
{
    public float updateDelay = 1f;
    
    public temp_range_t temperatureRange = new temp_range_t { min = -10f, max = 10f };

    private TemperatureEmitter temperatureEmitter;


    float Clip(float x, float min, float max) { return x < min ? min : x > max ? max : x; }

    float SunIntensity() {
        float x = DaytimeSystem.daytime > .5f ? 0 : DaytimeSystem.daytime * 2f;
        x = (.5f - Mathf.Abs(x - .5f)) * 4;
        return Clip(x, 0, 1);
    }


    IEnumerator UpdateTemperature() {
        temperatureEmitter.temperature = (temperatureRange.min + (temperatureRange.max - temperatureRange.min)) * SunIntensity();

        yield return new WaitForSeconds(updateDelay);
        StartCoroutine(UpdateTemperature());
    }


    void Awake() {
        temperatureEmitter = GetComponent<TemperatureEmitter>();
        temperatureEmitter.temperatureRange = temperatureRange;

        StartCoroutine(UpdateTemperature());
    }

}
