using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterSystem : MonoBehaviour
{
    public GameObject waterParticles;


    float fabs(float value) { return value < 0 ? -value : value; }
    float Clip(float x, float min, float max) { return x < min ? min : x > max ? max : x; }


    private void OnTriggerEnter(Collider other) {
        GameObject inst = Instantiate(waterParticles, other.transform.position, Quaternion.identity);
        ParticleSystem ps = inst.GetComponent<ParticleSystem>();

        float scale = Clip(0, 10, fabs(other.GetComponent<Rigidbody>().velocity.y) / 10);

        inst.transform.localScale = new Vector3(scale, scale, scale);
        ps.startSize *= scale;
        ps.startSpeed *= scale;

        StartCoroutine(DestroyParticles(inst, 1f));
    }


    IEnumerator DestroyParticles(GameObject inst, float delay) {
        yield return new WaitForSeconds(delay);
        Destroy(inst);
    }
}
