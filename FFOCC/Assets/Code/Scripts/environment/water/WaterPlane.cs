using UnityEngine;

public class WaterPlane : MonoBehaviour {
    public int size = 10;
    public int subdivisions = 10;

    void Start() {
        MeshFilter meshFilter = gameObject.AddComponent<MeshFilter>();
        MeshRenderer meshRenderer = GetComponent<MeshRenderer>();
        if (meshRenderer == null) meshRenderer = gameObject.AddComponent<MeshRenderer>();

        Mesh m = new Mesh();
        m.name = "ScriptedMesh";

        int hCount2 = subdivisions + 1;
        int vCount2 = subdivisions + 1;
        int numTriangles = subdivisions * subdivisions * 6;
        int numVertices = hCount2 * vCount2;

        Vector3[] vertices = new Vector3[numVertices];
        Vector2[] uv = new Vector2[numVertices];
        int[] triangles = new int[numTriangles];

        int index = 0;
        float uvFactorX = 1.0f / subdivisions;
        float uvFactorY = 1.0f / subdivisions;
        float scaleX = size * uvFactorX;
        float scaleY = size * uvFactorY;
        for (float y = 0.0f; y < vCount2; y++) {
            for (float x = 0.0f; x < hCount2; x++) {
                vertices[index] = new Vector3(x * scaleX - size / 2f, 0.0f, y * scaleY - size / 2f);
                uv[index++] = new Vector2(x * uvFactorX, y * uvFactorY);
            }
        }

        index = 0;
        for (int y = 0; y < subdivisions; y++) {
            for (int x = 0; x < subdivisions; x++) {
                triangles[index] = (y * hCount2) + x;
                triangles[index + 1] = ((y + 1) * hCount2) + x;
                triangles[index + 2] = (y * hCount2) + x + 1;

                triangles[index + 3] = ((y + 1) * hCount2) + x;
                triangles[index + 4] = ((y + 1) * hCount2) + x + 1;
                triangles[index + 5] = (y * hCount2) + x + 1;
                index += 6;
            }
        }

        m.vertices = vertices;
        m.uv = uv;
        m.triangles = triangles;
        m.RecalculateNormals();

        meshFilter.mesh = m;
        meshFilter.mesh.RecalculateBounds();
    }
}