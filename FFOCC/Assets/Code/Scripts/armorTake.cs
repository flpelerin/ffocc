using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class armorTake : MonoBehaviour
{
    public float armorValue;
    public PlayerStats player;
    public float armorDistance;

    private void Awake()
    {
        player = PlayerStats.FindObjectOfType<PlayerStats>();
    }
    // Update is called once per frame
    void Update()
    {
        if (Vector3.Distance(player.transform.position, gameObject.transform.position) < armorDistance)
        {
            //player.takeArmor(armorValue);
            Destroy(gameObject);
        }
    }
}
