using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class foodtake : MonoBehaviour
{
    public float foodValue;
    public PlayerStats player;
    public float foodDistance;
    public float drinkValue;

    private void Awake()
    {
        player = PlayerStats.FindObjectOfType<PlayerStats>();
    }
    // Update is called once per frame
    void Update()
    {
        if (Vector3.Distance(player.transform.position, gameObject.transform.position) < foodDistance)
        { 
            //player.takefood(foodValue,drinkValue);
            Destroy(gameObject);
        }
    }
}
