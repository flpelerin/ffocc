using UnityEngine;

public class LightsController : MonoBehaviour
{
    public float distanceMaximale = 20f;

    void Start()
    {
        InvokeRepeating("DesactiverLumieresEloigneesDuJoueur", 5f, 0.5f);
    }

    private void DesactiverLumieresEloigneesDuJoueur()
    {
        // Récupérer le composant Transform du joueur directement depuis le GameObject auquel le script est attaché
        Transform joueur = GetComponent<Transform>();

        if (joueur == null)
        {
            Debug.LogError("Le composant Transform du joueur n'a pas été trouvé.");
            return;
        }

        // Récupérer toutes les lumières dans la scène
        Light[] lumieres = FindObjectsOfType<Light>();

        // Parcourir toutes les lumières et les désactiver si elles sont éloignées du joueur
        foreach (Light lumiere in lumieres)
        {
            if(lumiere.type != LightType.Point)
            {
                continue;
            }
            if (Vector3.Distance(lumiere.transform.position, joueur.position) > distanceMaximale)
            {
                lumiere.enabled = false;
            }
            else
            {
                lumiere.enabled = true;
            }
        }
    }
}
