using MimicSpace;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using UnityEngine;

public class MimicSpawner : MonoBehaviour
{
    public GameObject mimicprefab,
                      mimicInstance = null;
    
    
    float Clip(float x, float min, float max) { return x < min ? min : x > max ? max : x; }

    float SunIntensity() {
        float x = DaytimeSystem.daytime > .5f ? 0 : DaytimeSystem.daytime * 2f;
        x = (.5f - Mathf.Abs(x - .5f)) * 4;
        return Clip(x, 0, 1);
    }


    // Update is called once per frame
    void Update() {
        if (SunIntensity() != 0 && mimicInstance != null) {
            Debug.Log("Destroying Mimic");

            Destroy(mimicInstance);
            mimicInstance = null;
        } else {
            if (mimicInstance == null) {
                Debug.Log("Spawning Mimic");

                mimicInstance = Instantiate(mimicprefab);
                mimicInstance.transform.position = new Vector3(Mathf.Lerp(-600, 1600, Mathf.InverseLerp(0, 1, Random.Range(0, 1f))),2000f, Mathf.Lerp(-1550, 620, Mathf.InverseLerp(0, 1, Random.Range(0, 1f))));

                LightEmetorMimic[] emitters = mimicInstance.GetComponentsInChildren<LightEmetorMimic>();
                foreach (LightEmetorMimic emitter in emitters)
                {
                    emitter.OnMimicSpawned();
                }
            }
        }
    }
}
