using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FogManagement : MonoBehaviour
{
    // Start is called before the first frame update
    public bool IsNight = false;
    public bool IsDay = false;
    public bool IsRainDay = false;
    public bool IsRainNight = false;
    void Start()
    {
        RenderSettings.fog = true;
        RenderSettings.fogColor = new Color(0.5f, 0.5f, 0.5f, 1.0f); //Brouillard gris
        RenderSettings.fogDensity = 0.01f;
        RenderSettings.ambientIntensity = 0.8f;
    }

    // Update is called once per frame
    void Update()
    {
        
        if (IsNight)
        {
            //Effet pleinne nuit
            RenderSettings.fogColor = new Color(0.0f, 0.0f, 0.0f, 1.0f); //Brouillard noir
            RenderSettings.fogDensity = 0.13f;
            RenderSettings.ambientIntensity = 0.2f;
        }

        if (IsDay)
        {
            //Effet jour normal
            RenderSettings.fogColor = new Color(0.5f, 0.5f, 0.5f, 1.0f); //Brouillard gris
            RenderSettings.fogDensity = 0.01f;
            RenderSettings.ambientIntensity = 0.8f;
        }
        if (IsRainDay)
        {
            RenderSettings.fogColor = new Color(0.3f, 0.3f, 0.4f, 1.0f); //Brouillard gris
            RenderSettings.fogDensity = 0.08f;
            RenderSettings.ambientIntensity = 0.6f;
        }
        if (IsRainNight)
        {
            RenderSettings.fogColor = new Color(0.01f, 0.01f, 0.1f, 1.0f); //Brouillard gris
            RenderSettings.fogDensity = 0.16f;
            RenderSettings.ambientIntensity = 0.1f;

        }
    }

    public void PlayerUnderDaWater(bool IsUnder)
    {
        Debug.Log("going under");
        if (IsUnder)
        {
            RenderSettings.fogColor = new Color(0.1f, 0.1f, 0.4f, 1.0f);
            RenderSettings.fogDensity = 0.85f;
        }
        else
        {
            // TODO : Envoie vers la fonction qui remet a l'heure du jour

            // Temporaire : 
            RenderSettings.fogColor = new Color(0.5f, 0.5f, 0.5f, 1.0f);
            RenderSettings.fogDensity = 0.02f;
        }
    }
}
