﻿using TMPro;
using UnityEngine;

public class PickUpItem : MonoBehaviour
{
    public Item item;
    private Inventory _inventory;
    private Camera _mainCamera;
    private ObjectOutline _outlineScript;
    public TextMeshProUGUI InteractUI;

    private GameObject m_player;
    float m_interactionDist = 5f;

    bool m_canInteract = false;

    bool _IsSelected = false;

    // Use this for initialization
    void Start()
    {
        _mainCamera = Camera.main;
        m_player = PlayerController.instance.gameObject;
        if (m_player != null)
        {
            _inventory = m_player.GetComponent<PlayerInventory>().m_inventory.GetComponent<Inventory>();
        }

        // Désactiver le script ObjectOutline.cs au départ
        _outlineScript = GetComponent<ObjectOutline>();
        if (_outlineScript != null)
        {
            _outlineScript.enabled = false;
        }

        InteractUI = GameObject.FindGameObjectWithTag("InteractUI").GetComponent<TextMeshProUGUI>();
    }

    void FixedUpdate()
    {
        if (Vector3.Distance(m_player.transform.position, transform.position) < m_interactionDist)
            m_canInteract = true;
        else
            m_canInteract= false;
    }

    // Update is called once per frame
    void Update()
    {
        if (_inventory != null && m_canInteract) 
        {
            Ray ray = _mainCamera.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0));
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider.gameObject == this.gameObject)
                {
                    _IsSelected = true;
                    if (_outlineScript != null)
                    {
                        _outlineScript.enabled = true;
                    }
                    if (InteractUI != null)
                    {
                        if (_inventory.ItemsInInventory.Count < (_inventory.width * _inventory.height))
                        {
                            InteractUI.text = "Appuyez sur E pour récupérer " + item.itemName;
                        }
                        else
                        {
                            InteractUI.text = "Vous n'avez plus de place dans votre inventaire";
                        }
                    }
                    if (Input.GetKeyDown(KeyCode.E))
                    {
                        if (_inventory.ItemsInInventory.Count < (_inventory.width * _inventory.height))
                        {
                            _inventory.addItemToInventory(item.itemID, item.itemValue);
                            _inventory.updateItemList();
                            _inventory.stackableSettings();
                            Destroy(gameObject);
                            if (InteractUI != null)
                            {
                                InteractUI.text = "";
                                _IsSelected = false;
                            }
                        }
                    }
                }
                else if (_IsSelected)
                {
                    if (InteractUI != null)
                    {
                        InteractUI.text = "";
                        _IsSelected = false;
                    }
                }
                else if (_outlineScript != null)
                    {
                        _outlineScript.enabled = false;
                    }
            }
        }
    }

}
