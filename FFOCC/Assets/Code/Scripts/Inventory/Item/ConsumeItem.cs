﻿using UnityEngine;
using UnityEngine.EventSystems;

public class ConsumeItem : MonoBehaviour, IPointerDownHandler
{
    public Item item;
    private static Tooltip tooltip;
    public ItemType[] itemTypeOfSlot;
    public static EquipmentSystem eS;
    public GameObject duplication;
    public static GameObject mainInventory;

    public static int EquippedWeaponID = -1;
    public static GameObject[] Weapons = null;

    GameObject m_player;
 
    void Start()
    {
        m_player = PlayerController.instance.gameObject;
        item = GetComponent<ItemOnObject>().item;
        if (tooltip == null)
            tooltip = GameObject.FindGameObjectWithTag("Tooltip").GetComponent<Tooltip>();
        if (eS == null)
            eS = m_player.GetComponent<PlayerInventory>().m_characterSystem.GetComponent<EquipmentSystem>();

        if (GameObject.FindGameObjectWithTag("MainInventory") != null)
            mainInventory = GameObject.FindGameObjectWithTag("MainInventory");

        itemTypeOfSlot = eS.itemTypeOfSlots;
    }

    public void OnPointerDown(PointerEventData data)
    {
        if (this.gameObject.transform.parent.parent.parent.GetComponent<EquipmentSystem>() == null)
        {
            bool gearable = false;
            Inventory inventory = transform.parent.parent.parent.GetComponent<Inventory>();

            if (data.button == PointerEventData.InputButton.Right)
            {
                if (item.itemType != ItemType.Weapon && item.itemType != ItemType.Ammo)
                {
                    tooltip.desactivateTooltip();
                }
                else
                {
                    return; 
                }
                //item from craft system to inventory
                if (transform.parent.GetComponent<CraftResultSlot>() != null)
                {
                    bool check = m_player.GetComponent<PlayerInventory>().m_inventory.GetComponent<Inventory>().checkIfItemAlreadyExist(item.itemID, item.itemValue);

                    if (!check)
                    {
                        m_player.GetComponent<PlayerInventory>().m_inventory.GetComponent<Inventory>().addItemToInventory(item.itemID, item.itemValue);
                        m_player.GetComponent<PlayerInventory>().m_inventory.GetComponent<Inventory>().stackableSettings();
                    }
                    CraftSystem cS = m_player.GetComponent<PlayerInventory>().m_craftSystem.GetComponent<CraftSystem>();
                    cS.deleteItems(item);
                    CraftResultSlot result = m_player.GetComponent<PlayerInventory>().m_craftSystem.transform.GetChild(3).GetComponent<CraftResultSlot>();
                    result.temp = 0;
                    tooltip.desactivateTooltip();
                    gearable = true;
                    GameObject.FindGameObjectWithTag("MainInventory").GetComponent<Inventory>().updateItemList();
                }
                else
                {
                    bool stop = false;
                    if (eS != null)
                    {
                        for (int i = 0; i < eS.slotsInTotal; i++)
                        {
                            if (itemTypeOfSlot[i].Equals(item.itemType))
                            {
                                if (eS.transform.GetChild(1).GetChild(i).childCount == 0)
                                {
                                    stop = true;
                                    if (eS.transform.GetChild(1).GetChild(i).parent.parent.GetComponent<EquipmentSystem>() != null && this.gameObject.transform.parent.parent.parent.GetComponent<EquipmentSystem>() != null) { }
                                    else
                                        inventory.EquiptItem(item);

                                    this.gameObject.transform.SetParent(eS.transform.GetChild(1).GetChild(i));
                                    this.gameObject.GetComponent<RectTransform>().localPosition = Vector3.zero;
                                    eS.gameObject.GetComponent<Inventory>().updateItemList();
                                    inventory.updateItemList();
                                    gearable = true;
                                    if (duplication != null)
                                        Destroy(duplication.gameObject);
                                    break;
                                }
                            }
                            // Si l'objet est une arme, on l'active sur le joueur pour qu'il puisse l'utiliser
                            else if (item.itemType == ItemType.Weapon)
                            {
                                return;
                            }


                        }


                        if (!stop)
                        {
                            for (int i = 0; i < eS.slotsInTotal; i++)
                            {
                                if (itemTypeOfSlot[i].Equals(item.itemType))
                                {
                                    if (eS.transform.GetChild(1).GetChild(i).childCount != 0)
                                    {
                                        GameObject otherItemFromCharacterSystem = eS.transform.GetChild(1).GetChild(i).GetChild(0).gameObject;
                                        Item otherSlotItem = otherItemFromCharacterSystem.GetComponent<ItemOnObject>().item;
                                        if (item.itemType == ItemType.UFPS_Weapon)
                                        {
                                            inventory.UnEquipItem1(otherItemFromCharacterSystem.GetComponent<ItemOnObject>().item);
                                            inventory.EquiptItem(item);
                                        }
                                        else
                                        {
                                            inventory.EquiptItem(item);
                                            if (item.itemType != ItemType.Backpack)
                                                inventory.UnEquipItem1(otherItemFromCharacterSystem.GetComponent<ItemOnObject>().item);
                                        }
                                        if (this == null)
                                        {
                                            GameObject dropItem = (GameObject)Instantiate(otherSlotItem.itemModel);
                                            dropItem.AddComponent<PickUpItem>();
                                            dropItem.GetComponent<PickUpItem>().item = otherSlotItem;
                                            dropItem.transform.localPosition = m_player.transform.localPosition + new Vector3(0, 1, 0);
                                            inventory.OnUpdateItemList();
                                        }
                                        else
                                        {
                                            otherItemFromCharacterSystem.transform.SetParent(this.transform.parent);
                                            otherItemFromCharacterSystem.GetComponent<RectTransform>().localPosition = Vector3.zero;
                                            if (this.gameObject.transform.parent.parent.parent.GetComponent<Hotbar>() != null)
                                                createDuplication(otherItemFromCharacterSystem);

                                            this.gameObject.transform.SetParent(eS.transform.GetChild(1).GetChild(i));
                                            this.gameObject.GetComponent<RectTransform>().localPosition = Vector3.zero;
                                        }                                        
                                        
                                        gearable = true;                                        
                                        if (duplication != null)
                                            Destroy(duplication.gameObject);
                                        eS.gameObject.GetComponent<Inventory>().updateItemList();
                                        inventory.OnUpdateItemList();
                                        break;
                                    }
                                }
                            }
                        }

                    }

                }
                if (!gearable && item.itemType != ItemType.UFPS_Ammo && item.itemType != ItemType.UFPS_Grenade)
                {

                    Item itemFromDup = null;
                    if (duplication != null)
                        itemFromDup = duplication.GetComponent<ItemOnObject>().item;

                    inventory.ConsumeAnItem(item);

                    item.itemValue--;
                    if (itemFromDup != null)
                    {
                        duplication.GetComponent<ItemOnObject>().item.itemValue--;
                        if (itemFromDup.itemValue <= 0)
                        {
                            if (tooltip != null)
                                tooltip.desactivateTooltip();
                            inventory.deleteItemFromInventory(item);
                            Destroy(duplication.gameObject); 
                        }
                    }
                    if (item.itemValue <= 0)
                    {
                        if (tooltip != null)
                            tooltip.desactivateTooltip();
                        inventory.deleteItemFromInventory(item);
                        Destroy(this.gameObject);                        
                    }

                }
                
            }
            

        }
    }    

    public void consumeIt()
    {
        Inventory inventory = transform.parent.parent.parent.GetComponent<Inventory>();

        bool gearable = false;

        if (GameObject.FindGameObjectWithTag("EquipmentSystem") != null)
            eS = m_player.GetComponent<PlayerInventory>().m_characterSystem.GetComponent<EquipmentSystem>();

        if (eS != null)
            itemTypeOfSlot = eS.itemTypeOfSlots;

        Item itemFromDup = null;
        if (duplication != null)
            itemFromDup = duplication.GetComponent<ItemOnObject>().item;

        bool stop = false;
        if (eS != null)
        {
            for (int i = 0; i < eS.slotsInTotal; i++)
            {
                if (itemTypeOfSlot[i].Equals(item.itemType))
                {
                    if (eS.transform.GetChild(1).GetChild(i).childCount == 0)
                    {
                        stop = true;
                        this.gameObject.transform.SetParent(eS.transform.GetChild(1).GetChild(i));
                        this.gameObject.GetComponent<RectTransform>().localPosition = Vector3.zero;
                        eS.gameObject.GetComponent<Inventory>().updateItemList();
                        inventory.updateItemList();
                        inventory.EquiptItem(item);
                        gearable = true;
                        if (duplication != null)
                            Destroy(duplication.gameObject);
                        break;
                    }
                }
            }

            if (!stop)
            {
                for (int i = 0; i < eS.slotsInTotal; i++)
                {
                    if (itemTypeOfSlot[i].Equals(item.itemType))
                    {
                        if (eS.transform.GetChild(1).GetChild(i).childCount != 0)
                        {
                            GameObject otherItemFromCharacterSystem = eS.transform.GetChild(1).GetChild(i).GetChild(0).gameObject;
                            Item otherSlotItem = otherItemFromCharacterSystem.GetComponent<ItemOnObject>().item;
                            if (item.itemType == ItemType.UFPS_Weapon)
                            {
                                inventory.UnEquipItem1(otherItemFromCharacterSystem.GetComponent<ItemOnObject>().item);
                                inventory.EquiptItem(item);
                            }
                            else
                            {
                                inventory.EquiptItem(item);
                                if (item.itemType != ItemType.Backpack)
                                    inventory.UnEquipItem1(otherItemFromCharacterSystem.GetComponent<ItemOnObject>().item);
                            }
                            if (this == null)
                            {
                                GameObject dropItem = (GameObject)Instantiate(otherSlotItem.itemModel);
                                dropItem.AddComponent<PickUpItem>();
                                dropItem.GetComponent<PickUpItem>().item = otherSlotItem;
                                dropItem.transform.localPosition = m_player.transform.localPosition + new Vector3(0, 1, 0);
                                inventory.OnUpdateItemList();
                            }
                            else
                            {
                                otherItemFromCharacterSystem.transform.SetParent(this.transform.parent);
                                otherItemFromCharacterSystem.GetComponent<RectTransform>().localPosition = Vector3.zero;
                                if (this.gameObject.transform.parent.parent.parent.GetComponent<Hotbar>() != null)
                                    createDuplication(otherItemFromCharacterSystem);

                                this.gameObject.transform.SetParent(eS.transform.GetChild(1).GetChild(i));
                                this.gameObject.GetComponent<RectTransform>().localPosition = Vector3.zero;
                            }

                            gearable = true;
                            if (duplication != null)
                                Destroy(duplication.gameObject);
                            eS.gameObject.GetComponent<Inventory>().updateItemList();
                            inventory.OnUpdateItemList();
                            break;                           
                        }
                    }
                }
            }


        }
        if (!gearable && item.itemType != ItemType.UFPS_Ammo && item.itemType != ItemType.UFPS_Grenade)
        {
            if (item.itemType == ItemType.Weapon)
            {
                if (item.itemID != EquippedWeaponID)
                {
                    if (item.itemID <= Weapons.Length)
                    {
                        foreach (GameObject weapon in Weapons)
                        {
                            weapon.SetActive(false);
                                    //if (weapon.GetComponent<WeaponManager>())
                                    //    weapon.GetComponent<WeaponManager>().m_LockFire = true;
                                    //else if (weapon.GetComponentInChildren<MeleeWeapon>())
                                    //    weapon.GetComponentInChildren<MeleeWeapon>().m_LockAttack = true;
                            if (weapon.GetComponentInChildren<Lighter>())
                                weapon.GetComponentInChildren<Lighter>().Locktoogle = true;
                            EquippedWeaponID = -1;
                                Weapons[item.itemID].SetActive(false);
                            
                        }
                        Weapons[item.itemID].SetActive(true);
                        PlayerController.instance.m_weapon = Weapons[item.itemID];
                        
                        EquippedWeaponID = item.itemID;
                    }
                    else
                    {
                        Debug.Log("Arme non supportée");
                    }
                }
                else
                {
                    if (Weapons[item.itemID].GetComponentInChildren<Lighter>())
                    {
                        StartCoroutine(Weapons[item.itemID].GetComponentInChildren<Lighter>().OutsideClose());
                    }
                    //else
                        Weapons[item.itemID].SetActive(false);
                    EquippedWeaponID = -1;
                }
                return;
            }
            if (duplication != null)
                itemFromDup = duplication.GetComponent<ItemOnObject>().item;

            inventory.ConsumeAnItem(item);


            item.itemValue--;
            if (itemFromDup != null)
            {
                duplication.GetComponent<ItemOnObject>().item.itemValue--;
                if (itemFromDup.itemValue <= 0)
                {
                    if (tooltip != null)
                        tooltip.desactivateTooltip();
                    inventory.deleteItemFromInventory(item);
                    Destroy(duplication.gameObject);

                }
            }
            if (item.itemValue <= 0)
            {
                if (tooltip != null)
                    tooltip.desactivateTooltip();
                inventory.deleteItemFromInventory(item);
                Destroy(this.gameObject); 
            }

        }        
    }

    public void DestroyItem()
    {
        Destroy(this.gameObject);
    }
    public void createDuplication(GameObject Item)
    {
        Item item = Item.GetComponent<ItemOnObject>().item;
        GameObject dup = mainInventory.GetComponent<Inventory>().addItemToInventory(item.itemID, item.itemValue);
        Item.GetComponent<ConsumeItem>().duplication = dup;
        dup.GetComponent<ConsumeItem>().duplication = Item;
    }
}
