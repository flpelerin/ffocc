﻿using System.Collections.Generic;
using UnityEngine;

public class PlayerInventory : MonoBehaviour
{
    public GameObject m_inventory;
    public GameObject m_characterSystem;
    public GameObject m_craftSystem;
    private Inventory m_craftSystemInventory;
    private CraftSystem m_craftSystem_S;
    private Inventory m_mainInventory;
    private Inventory m_characterSystemInventory;
    private Tooltip m_toolTip;

    private InputManager m_inputManagerDatabase;


    int normalSize = 3; // taille initiale de l'inventaire 

    public void OnEnable()
    {
        Inventory.ItemEquip += OnBackpack;
        Inventory.UnEquipItem += UnEquipBackpack;

        Inventory.ItemEquip += OnGearItem;
        Inventory.ItemConsumed += OnConsumeItem;
        Inventory.UnEquipItem += OnUnEquipItem;

        Inventory.ItemEquip += EquipWeapon;
        Inventory.UnEquipItem += UnEquipWeapon;
    }

    public void OnDisable()
    {
        Inventory.ItemEquip -= OnBackpack;
        Inventory.UnEquipItem -= UnEquipBackpack;

        Inventory.ItemEquip -= OnGearItem;
        Inventory.ItemConsumed -= OnConsumeItem;
        Inventory.UnEquipItem -= OnUnEquipItem;

        Inventory.UnEquipItem -= UnEquipWeapon;
        Inventory.ItemEquip -= EquipWeapon;
    }

    void EquipWeapon(Item item)
    {
        if (item.itemType == ItemType.Weapon)
        {
            //add the weapon if you unequip the weapon
        }
    }

    void UnEquipWeapon(Item item)
    {
        if (item.itemType == ItemType.Weapon)
        {
            //delete the weapon if you unequip the weapon
        }
    }

    public List<Item> GetAmmos()
    {
        return m_mainInventory.GetAmmos();
    }

    public void deleteItemFromInventoryWithGameObject(Item item)
    {
        m_mainInventory.deleteItemFromInventoryWithGameObject(item);
    }

    void OnBackpack(Item item)
    {
        if (item.itemType == ItemType.Backpack)
        {
            for (int i = 0; i < item.itemAttributes.Count; i++)
            {
                if (m_mainInventory == null)
                    m_mainInventory = m_inventory.GetComponent<Inventory>();
                m_mainInventory.sortItems();
                if (item.itemAttributes[i].attributeName == "Slots")
                {
                    changeInventorySize(item.itemAttributes[i].attributeValue);
                }
            }
        }
    }

    void UnEquipBackpack(Item item)
    {
        if (item.itemType == ItemType.Backpack)
            changeInventorySize(normalSize);
    }

    void changeInventorySize(int size)
    {
        dropTheRestItems(size);

        if (m_mainInventory == null)
            m_mainInventory = m_inventory.GetComponent<Inventory>();
        switch (size)
        {
            case 1:
                m_mainInventory.width = 1;
                m_mainInventory.height = 1;
                m_mainInventory.updateSlotAmount();
                m_mainInventory.adjustInventorySize();
                break;
            case 2:
                m_mainInventory.width = 2;
                m_mainInventory.height = 1;
                m_mainInventory.updateSlotAmount();
                m_mainInventory.adjustInventorySize();
                break;
            case 3:
                m_mainInventory.width = 3;
                m_mainInventory.height = 1;
                m_mainInventory.updateSlotAmount();
                m_mainInventory.adjustInventorySize();
                break;
            case 4:
                m_mainInventory.width = 2;
                m_mainInventory.height = 2;
                m_mainInventory.updateSlotAmount();
                m_mainInventory.adjustInventorySize();
                break;
            case 6:
                m_mainInventory.width = 3;
                m_mainInventory.height = 2;
                m_mainInventory.updateSlotAmount();
                m_mainInventory.adjustInventorySize();
                break;
            case 8:
                m_mainInventory.width = 4;
                m_mainInventory.height = 2;
                m_mainInventory.updateSlotAmount();
                m_mainInventory.adjustInventorySize();
                break;
            case 9:
                m_mainInventory.width = 3;
                m_mainInventory.height = 3;
                m_mainInventory.updateSlotAmount();
                m_mainInventory.adjustInventorySize();
                break;
            case 10:
                m_mainInventory.width = 5;
                m_mainInventory.height = 2;
                m_mainInventory.updateSlotAmount();
                m_mainInventory.adjustInventorySize();
                break;
            case 12:
                m_mainInventory.width = 4;
                m_mainInventory.height = 3;
                m_mainInventory.updateSlotAmount();
                m_mainInventory.adjustInventorySize();
                break;
            case 14:
                m_mainInventory.width = 7;
                m_mainInventory.height = 2;
                m_mainInventory.updateSlotAmount();
                m_mainInventory.adjustInventorySize();
                break;
            case 15:
                m_mainInventory.width = 5;
                m_mainInventory.height = 3;
                m_mainInventory.updateSlotAmount();
                m_mainInventory.adjustInventorySize();
                break;
            case 16:
                m_mainInventory.width = 4;
                m_mainInventory.height = 4;
                m_mainInventory.updateSlotAmount();
                m_mainInventory.adjustInventorySize();
                break;
            case 18:
                m_mainInventory.width = 6;
                m_mainInventory.height = 3;
                m_mainInventory.updateSlotAmount();
                m_mainInventory.adjustInventorySize();
                break;
            case 20:
                m_mainInventory.width = 5;
                m_mainInventory.height = 4;
                m_mainInventory.updateSlotAmount();
                m_mainInventory.adjustInventorySize();
                break;
            case 21:
                m_mainInventory.width = 7;
                m_mainInventory.height = 3;
                m_mainInventory.updateSlotAmount();
                m_mainInventory.adjustInventorySize();
                break;
            case 24:
                m_mainInventory.width = 6;
                m_mainInventory.height = 4;
                m_mainInventory.updateSlotAmount();
                m_mainInventory.adjustInventorySize();
                break;
            default:
                print("Taille de backpack non supportée");
                break;

        }
    }

    void dropTheRestItems(int size)
    {
        if (size < m_mainInventory.ItemsInInventory.Count)
        {
            for (int i = size; i < m_mainInventory.ItemsInInventory.Count; i++)
            {
                GameObject dropItem = (GameObject)Instantiate(m_mainInventory.ItemsInInventory[i].itemModel);
                dropItem.AddComponent<PickUpItem>();
                dropItem.GetComponent<PickUpItem>().item = m_mainInventory.ItemsInInventory[i];
                dropItem.transform.localPosition = PlayerController.instance.transform.localPosition + new Vector3(0,1.5f,0);
            }
        }
    }

    void Start()
    {
        if (m_inputManagerDatabase == null)
            m_inputManagerDatabase = (InputManager)Resources.Load("InputManager");

        if (m_craftSystem != null)
            m_craftSystem_S = m_craftSystem.GetComponent<CraftSystem>();

        if (GameObject.FindGameObjectWithTag("Tooltip") != null)
            m_toolTip = GameObject.FindGameObjectWithTag("Tooltip").GetComponent<Tooltip>();
        if (m_inventory != null)
            m_mainInventory = m_inventory.GetComponent<Inventory>();
        if (m_characterSystem != null)
            m_characterSystemInventory = m_characterSystem.GetComponent<Inventory>();
        if (m_craftSystem != null)
            m_craftSystemInventory = m_craftSystem.GetComponent<Inventory>();
    }




    public void OnConsumeItem(Item item)
    {
        //for (int i = 0; i < item.itemAttributes.Count; i++)
        //{
        //    if (item.itemAttributes[i].attributeName == "Health")
        //    {
        //        if ((currentHealth + item.itemAttributes[i].attributeValue) > maxHealth)
        //            currentHealth = maxHealth;
        //        else
        //            currentHealth += item.itemAttributes[i].attributeValue;
        //    }
        //    if (item.itemAttributes[i].attributeName == "Mana")
        //    {
        //        if ((currentMana + item.itemAttributes[i].attributeValue) > maxMana)
        //            currentMana = maxMana;
        //        else
        //            currentMana += item.itemAttributes[i].attributeValue;
        //    }
        //    if (item.itemAttributes[i].attributeName == "Armor")
        //    {
        //        if ((currentArmor + item.itemAttributes[i].attributeValue) > maxArmor)
        //            currentArmor = maxArmor;
        //        else
        //            currentArmor += item.itemAttributes[i].attributeValue;
        //    }
        //    if (item.itemAttributes[i].attributeName == "Damage")
        //    {
        //        if ((currentDamage + item.itemAttributes[i].attributeValue) > maxDamage)
        //            currentDamage = maxDamage;
        //        else
        //            currentDamage += item.itemAttributes[i].attributeValue;
        //    }
        //}
    }

    public void OnGearItem(Item item)
    {
        //for (int i = 0; i < item.itemAttributes.Count; i++)
        //{
        //    if (item.itemAttributes[i].attributeName == "Health")
        //        maxHealth += item.itemAttributes[i].attributeValue;
        //    if (item.itemAttributes[i].attributeName == "Mana")
        //        maxMana += item.itemAttributes[i].attributeValue;
        //    if (item.itemAttributes[i].attributeName == "Armor")
        //        maxArmor += item.itemAttributes[i].attributeValue;
        //    if (item.itemAttributes[i].attributeName == "Damage")
        //        maxDamage += item.itemAttributes[i].attributeValue;
        //}
        ////if (HPMANACanvas != null)
        ////{
        ////    UpdateManaBar();
        ////    UpdateHPBar();
        ////}
    }

    public void OnUnEquipItem(Item item)
    {
        //for (int i = 0; i < item.itemAttributes.Count; i++)
        //{
        //    if (item.itemAttributes[i].attributeName == "Health")
        //        maxHealth -= item.itemAttributes[i].attributeValue;
        //    if (item.itemAttributes[i].attributeName == "Mana")
        //        maxMana -= item.itemAttributes[i].attributeValue;
        //    if (item.itemAttributes[i].attributeName == "Armor")
        //        maxArmor -= item.itemAttributes[i].attributeValue;
        //    if (item.itemAttributes[i].attributeName == "Damage")
        //        maxDamage -= item.itemAttributes[i].attributeValue;
        //}
        ////if (HPMANACanvas != null)
        ////{
        ////    UpdateManaBar();
        ////    UpdateHPBar();
        ////}
    }



    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(m_inputManagerDatabase.CharacterSystemKeyCode))
        {
            if (!m_characterSystem.activeSelf)
            {
                m_characterSystemInventory.openInventory();
            }
            else
            {
                if (m_toolTip != null)
                    m_toolTip.desactivateTooltip();
                m_characterSystemInventory.closeInventory();
            }
        }

        if (Input.GetKeyDown(m_inputManagerDatabase.InventoryKeyCode))
        {
            if (!m_inventory.activeSelf)
            {
                m_mainInventory.openInventory();
            }
            else
            {
                if (m_toolTip != null)
                    m_toolTip.desactivateTooltip();
                m_mainInventory.closeInventory();
            }
        }

        if (Input.GetKeyDown(m_inputManagerDatabase.CraftSystemKeyCode))
        {
            if (!m_craftSystem.activeSelf)
                m_craftSystemInventory.openInventory();
            else
            {
                if (m_craftSystem_S != null)
                    m_craftSystem_S.backToInventory();
                if (m_toolTip != null)
                    m_toolTip.desactivateTooltip();
                m_craftSystemInventory.closeInventory();
            }
        }

    }

}
