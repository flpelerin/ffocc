﻿using MimicSpace;
using System.Collections;
using UnityEngine;


public class Movement : MonoBehaviour
{
    [Tooltip("Body Height from ground")]
    [Range(0.5f, 5f)]
    public float height = 0.8f;
    public float Maxheight = 0.2f;
    public float Minheight = 2f;

    public float AgroDist = 15.0f;
    public float RememberFire = 30f;

    public float speed;
    [Tooltip("Vitesse déplacement, vitesse attaque de +3")]
    public float Maxspeed = 8f;
    public float Minspeed = 1.5f;
    public float DistanceToKeep = 3f;
    public float DistanceTurnaround;
    Vector3 velocity = Vector3.zero;
    public float velocityLerpCoef = 4f;
    Mimic myMimic;
    public Transform targetObject; // Le GameObject vers lequel vous voulez vous déplacer
    public GameObject Player;
    public float AgroDistConst;
    bool inIdle = false;
    private float randomDirX;
    private float randomDirY;
    private float size;
    private float distanceToPlayer;
    private float distanceToTarget;

    private void Start()
    {
        Player = GameObject.FindWithTag("Player");
        myMimic = GetComponent<Mimic>();
        AgroDistConst = AgroDist;
        TrouverObjetLePlusProche();
        size = this.transform.localScale.x;
    }

    void FixedUpdate()
    {
        // Calculer la distance entre l'objet et la cible
        distanceToPlayer = (Player.transform.position - transform.position).magnitude;
        if (targetObject != null)
        {
            distanceToTarget = (targetObject.position - transform.position).magnitude;
            if (AgroDist >= distanceToPlayer)
            {
                if (RememberFire >= distanceToTarget)
                {
                    height = Mathf.Lerp(Minheight, Maxheight, Mathf.InverseLerp(1f, AgroDistConst, distanceToPlayer));
                    speed = Mathf.Lerp(Minspeed + 3, Maxspeed + 3, Mathf.InverseLerp(1f, 50f, distanceToPlayer));
                    AttackPlayer();
                }
                else
                {
                    StartCoroutine(Retarget());
                }

            }
            else
            {
                height = Mathf.Lerp(Minheight, Maxheight, Mathf.InverseLerp(2f, 20f, distanceToTarget));
                speed = Mathf.Lerp(Minspeed, Maxspeed, Mathf.InverseLerp(2f, 40f, distanceToTarget));
                if (distanceToTarget < DistanceTurnaround)
                {
                    TurnARound();
                }
                else
                {
                    MoveToTarget();
                }
            }
        }
        else 
        { 
            if(!inIdle) { StartCoroutine(IdleMove());}

            if (AgroDist/2 >= distanceToPlayer )
            {

                height = Mathf.Lerp(Minheight, Maxheight, Mathf.InverseLerp(1f, AgroDistConst, distanceToPlayer));
                speed = Mathf.Lerp(Minspeed + 3, Maxspeed + 3, Mathf.InverseLerp(1f, 50f, distanceToPlayer));
                AttackPlayer();
            }
            else
            {

                MoveToTarget();
            }
        }

        RaycastHit hit;
        Vector3 destHeight = transform.position;

        // Le code suivant sera exécuté pour tous les objets sauf le "Player"
        if (Physics.Raycast(transform.position + Vector3.up * 5f, -Vector3.up, out hit))
        {
            // Exclure le "Player" de l'interaction
            if (!hit.collider.CompareTag("Player"))
            {
                destHeight = new Vector3(transform.position.x, hit.point.y + height, transform.position.z);
            }
        }

        transform.position = Vector3.Lerp(transform.position, destHeight, velocityLerpCoef * Time.deltaTime);
    }

    private void TurnARound()
    {
        // Calculer la direction vers le GameObject cible
        Vector3 directionToTarget = (targetObject.position - transform.position).normalized;

        // Ajouter un décalage basé sur DistanceToKeep
        Vector3 targetPosition = targetObject.position - directionToTarget * DistanceToKeep;

        // Calculer la rotation nécessaire pour faire face à la cible
        Quaternion targetRotation = Quaternion.LookRotation(targetPosition - transform.position);

        // Tourner progressivement vers la rotation cible
        transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, velocityLerpCoef * Time.deltaTime);

        // Calculer la vélocité en utilisant la direction et la vitesse
        velocity = Vector3.Lerp(velocity, transform.forward * speed, velocityLerpCoef * Time.deltaTime);

        // Assigner la vélocité à l'objet Mimic
        myMimic.velocity = velocity;

        // Mettre à jour la position de l'objet en fonction de la vélocité
        transform.position = transform.position + velocity * Time.deltaTime;
    }


    private void MoveToTarget()
    {
        this.transform.localScale = new Vector3(1, 1, 1);
        Vector3 directionToTarget;
        if (inIdle)
        {
            // Condition de trop loin à implémenté
           /* if(this.position.x < -600 or this.position.x > 1600 or this.position.y > 620 or this.position.y < -1550)
            {

            }*/
            speed = 5f;
            height = 1.5f;
            directionToTarget = new Vector3(randomDirX, 0f, randomDirY).normalized;
        }
        else
        {
            // Calculer la direction vers le GameObject cible
            directionToTarget = (targetObject.position - transform.position).normalized;
            Debug.Log(directionToTarget);
        }
        // Calculer la vélocité en utilisant la direction et la vitesse
        velocity = Vector3.Lerp(velocity, directionToTarget * speed, velocityLerpCoef * Time.deltaTime);
        // Assigner la vélocité à l'objet Mimic
        myMimic.velocity = velocity;

        // Mettre à jour la position de l'objet en fonction de la vélocité
        transform.position = transform.position + velocity * Time.deltaTime;

    }

    private void AttackPlayer()
    {
        // Calculer la direction vers le GameObject cible
        Vector3 directionToPlayer = (Player.transform.position - transform.position).normalized;

        // Calculer la vélocité en utilisant la direction et la vitesse
        velocity = Vector3.Lerp(velocity, directionToPlayer * speed, velocityLerpCoef * Time.deltaTime);

        // Assigner la vélocité à l'objet Mimic
        myMimic.velocity = velocity;

        // Mettre à jour la position de l'objet en fonction de la vélocité
        transform.position = transform.position + velocity * Time.deltaTime;

        size = Mathf.Lerp(1, 5, Mathf.InverseLerp(5f, 0f, distanceToPlayer));
        print(distanceToPlayer);
        this.transform.localScale = new Vector3(size, size, size);
        if(distanceToPlayer< 2) 
        {
            Player.GetComponent<PlayerStats>().TakeDamage(1);
        }
    }

    public void TrouverObjetLePlusProche()
    {
        targetObject = null;
        // Trouver tous les objets dans la scène contenant le script LightEmitterMimic
        LightEmetorMimic[] objetsAvecScript = FindObjectsOfType<LightEmetorMimic>();
        LightEmetorMimic ObjetLePlusProche;
        print("Passage 1");
        // Si aucun objet trouvé, sortir de la fonction
        if (objetsAvecScript.Length == 0)
        {
            return;
        }

        // Initialiser la distance minimale avec une valeur élevée
        float distanceMinimale = Mathf.Infinity;
        print("Passage 2");
        // Boucler à travers tous les objets avec le script
        foreach (LightEmetorMimic objet in objetsAvecScript)
        {
            // Vérifier si IsLight est à true pour l'objet actuel avec le script
            print("Passage 3");
            if (objet.IsLight)
            {
                // Calculer la distance entre l'objet courant et l'objet actuel avec le script
                float distance = Vector3.Distance(transform.position, objet.transform.position);
                print("Passage 4");
                // Mettre à jour l'objet le plus proche si la distance est inférieure à la distance minimale actuelle
                if (distance < distanceMinimale)
                {
                    distanceMinimale = distance;
                    ObjetLePlusProche = objet;
                    targetObject = ObjetLePlusProche.transform;
                    print("Passage 5");
                }
            }
        }
    }

    IEnumerator IdleMove()
    {
        randomDirX = Random.Range(-1f, 1f);
        
        randomDirY = Random.Range(-1f, 1f);
        inIdle = true;
        yield return new WaitForSeconds(10f);
        inIdle = false;
    }
    IEnumerator Retarget()
    {
        AgroDist = 0;
        yield return new WaitForSeconds(5f);
        AgroDist = AgroDistConst;
    }
}

