using System.Collections;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UIElements;

public class ZombieAI : MonoBehaviour
{
    public NavMeshAgent agent;

    public Transform player;

    public LayerMask isGround, isPlayer;

    public float health;

    bool canHit = false;

    Animator m_animator;
    Collider m_collider;

    private float waitDelay;

    [Header("Patrouille")]
    public Vector3 walkPoint;
    bool walkPointSet;
    public float walkPointRange;

    private Vector3 previousPosition;
    private float timeSinceLastDestinationChange;

    [Header("Attaque")]
    public float timeBetweenAttack;
    bool alreadyAttacked;

    [Header("?tats")]
    public float sightRange, attackRange;
    public bool playerInSightRange, playerInAttackRange;

    private void Start()
    {
        player = PlayerController.instance.transform;
        agent = GetComponent<NavMeshAgent>();
        m_animator = GetComponent<Animator>();
        m_collider = GetComponent<Collider>();

        isGround = LayerMask.GetMask("isGround");
        isPlayer = LayerMask.GetMask("isPlayer");

        waitDelay = Random.Range(.5f, 1.5f);

        StartCoroutine(VerifPath());
    }

    private void Update()
    {
        if (agent.isStopped == false)
        {
            m_animator.SetFloat("MoveSpeed", Mathf.Abs(agent.velocity.x) + Mathf.Abs(agent.velocity.z));
        }
    }

    IEnumerator VerifPath()
    {
        if (agent.isStopped == false)
        {
            playerInSightRange = Physics.CheckSphere(transform.position, sightRange, isPlayer);
            playerInAttackRange = Physics.CheckSphere(transform.position, attackRange, isPlayer);

            // V?rification du bon d?placement du zombie vers sa destination
            if (transform.position != previousPosition)
            {
                timeSinceLastDestinationChange = 0f;
                previousPosition = transform.position;
            }
            else
            {
                timeSinceLastDestinationChange += Time.deltaTime;
            }

            // Si le Zombie est bloqu?/n'arrive pas ? arriver ? destination ? cause
            // d'un endroit ferm? par exemple, on change de destination en en prenant une nouvelle
            if (timeSinceLastDestinationChange > 2f)
            {
                ResetDestination();
            }

            if (!playerInSightRange && !playerInAttackRange)// Si le zombie est loin du joueur, et ne peut donc pas l'attaquer, il patrouille al?atoirement
            {
                Patrolling();
            }
            if (playerInSightRange && !playerInAttackRange) // Si le zombie est proche du jouer, mais pas assez pour l'attaquer
            {
                NavMeshPath path = new NavMeshPath();
                agent.CalculatePath(player.position, path);

                // On calcule le chemin, et on va ensuite effectuer des v?rifications avec
                switch (path.status)
                {
                    case NavMeshPathStatus.PathComplete:    // Si le chemin est complet, le zombie va poursuivre le joueur
                        ChasePlayer();
                        break;
                    default:                                // Si le chemin est partiel ou impossible, le zombie va simplement patrouiller
                        Patrolling();
                        break;
                }
            }
            if (playerInSightRange && playerInAttackRange)  // Si le zombie est assez proche du joueur pour pouvoir l'attaquer, il l'attaque
            {
                AttackPlayer();
            }
        }

        yield return new WaitForSeconds(waitDelay);
        StartCoroutine(VerifPath());
    }

    private void Patrolling()
    {
        if (!walkPointSet)
            SearchWalkPoint();

        if (walkPointSet)
            agent.SetDestination(walkPoint);

        Vector3 distanceToWalkPoint = transform.position - walkPoint;

        if (distanceToWalkPoint.magnitude < 1f)
            walkPointSet = false;
    }
    private void SearchWalkPoint()      // Fonction pour calculer une nouvelle destination
    {
        // Calcul d'un chemin de patrouille al?atoire
        float randomZ = UnityEngine.Random.Range(-walkPointRange, walkPointRange);
        float randomX = UnityEngine.Random.Range(-walkPointRange, walkPointRange);

        walkPoint = new Vector3(transform.position.x + randomX, transform.position.y, transform.position.z + randomZ);

        if (Physics.Raycast(walkPoint, -transform.up, 2f, isGround))
            walkPointSet = true;
    }

    private void ResetDestination()     // Fonction r?initialisant la destination car elle est atteinte
    {
        walkPointSet = false;
    }

    private void ChasePlayer()
    {
        agent.SetDestination(player.position);
    }

    private void AttackPlayer()         // Fonction pour g?rer l'attaque du zombie envers le joueur
    {
        if (agent.isStopped == false)
        {
            agent.SetDestination(transform.position);

            // Calcul de la direction vers le joueur
            Vector3 directionToPlayer = (player.position - transform.position).normalized;

            // Calcul de la rotation pour faire face au joueur
            Quaternion lookRotation = Quaternion.LookRotation(new Vector3(directionToPlayer.x, 0f, directionToPlayer.z));

            // Transition plus douce
            transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 5f);

            if (!alreadyAttacked)
            {
                canHit = true;
                m_animator.SetTrigger("Attack");
                // Il faudra mettre les dommages au joueur ici
                Debug.Log("J'attaque l�");
                alreadyAttacked = true;
                Invoke(nameof(ResetAttack), timeBetweenAttack);
            }
        }
    }

    private void ResetAttack()          // Fonction qui permets au Zombie d'attaquer ? nouveau apr?s son cooldown
    {
        alreadyAttacked = false;
    }

    public void TakeDamage(int damage)  // Fonction qui inflige des d?g?ts au Zombie (et v?rifie si il est mort)
    {
        health -= damage;

        if (health <= 0)                        // Si jamais le joueur est mort
        {
            m_animator.SetTrigger("Death");     // On joue l'aimatiion de mort su zombie
            agent.isStopped = true;             // On arr?te tout d?placement de l'IA
            Destroy(m_collider);                // On supprime son collider pour optimiser le tout
            Invoke(nameof(DestroyEnemmy), 30f); // On d?truira son corps dans 60 secondes, afin de voir le corps, mais d'optimiser le jeu pour ?viter de garder tous les coprs
        }
    }

    private void DestroyEnemmy()        // Fonction d?truisant au Zombie
    {
        Destroy(gameObject);
    }

    public void HitPlayer()
    {
        if (alreadyAttacked && canHit)
        {
            canHit = false;
            player.GetComponent<PlayerStats>().TakeDamage(10);
            Debug.Log("Tu es touch�");
        }
    }

    public bool CanHit()
    {
        return canHit;
    }

}