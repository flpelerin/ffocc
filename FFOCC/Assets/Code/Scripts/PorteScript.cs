    using System.Collections;
    using Unity.VisualScripting;
    using UnityEngine;

    public class PorteScript : MonoBehaviour
    {
        public float maxAngle = 90;
        private float targetAngle;
        private float currentAngle;

        public bool isOpen = false;
        public bool isMoving = false;

        void Awake()
        {
            currentAngle = transform.rotation.eulerAngles.y;
            targetAngle = currentAngle;
        }

        void Update()
        {
            if (Input.GetKeyDown(KeyCode.E) && !isMoving)
            {
                Debug.Log("E key pressed");
                Vector3 playerPosition = GameObject.FindGameObjectWithTag("Player").transform.position;
                float distance = Vector3.Distance(playerPosition, transform.position);
                if (distance <= 2f)
                {
                    Debug.Log("Player is near the door");
                    if (!isOpen)
                    {
                        isMoving = true;
                        isOpen = true;
                        Debug.Log("Opening door");
                        StartCoroutine(OpenDoor());
                    }
                    else
                    {
                        isMoving = true;
                        isOpen = false;
                        Debug.Log("Closing door");
                        StartCoroutine(CloseDoor());
                    }
                }
            }
        }

        IEnumerator OpenDoor()
        {
            targetAngle = currentAngle + maxAngle;
            while (currentAngle < targetAngle)
            {
                currentAngle += 1;
                transform.rotation = Quaternion.Euler(0, currentAngle, 0);
                yield return new WaitForSeconds(0.005f);
            }
            isMoving = false;
            yield return null;
        }

        IEnumerator CloseDoor()
        {
            targetAngle = currentAngle - maxAngle;
            while (currentAngle > targetAngle)
            {
                currentAngle -= 1;
                transform.rotation = Quaternion.Euler(0, currentAngle, 0);
                yield return new WaitForSeconds(0.005f);
            }
            isMoving = false;
            yield return null;
        }
    }