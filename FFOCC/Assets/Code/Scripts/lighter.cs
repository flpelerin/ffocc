using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lighter : MonoBehaviour
{
    private LightEmetorMimic MyLighter;
    public Light MyLight;
    public Animator FirstAnimator;
    public Animator SecondAnimator;
    public ParticleSystem.EmissionModule emissionModule;

    public bool Locktoogle = true;
    bool open = false;
    [SerializeField] float m_opening_cooldown = 1.2f;

    float m_timesinceswitch = 0f ;

    private void Start()
    {
        emissionModule = GetComponentInChildren<ParticleSystem>().emission;
        MyLighter = GetComponentInChildren<LightEmetorMimic>();
        MyLight = GetComponentInChildren<Light>();
        emissionModule.enabled = false;
    }

    void Update()
    {
        m_timesinceswitch += Time.deltaTime;
        if (Locktoogle)
            return;
        if (Input.GetMouseButton(0) && m_timesinceswitch > m_opening_cooldown)
        {
            m_timesinceswitch = 0f;
            if (!open)
                StartCoroutine(Open());
            else
                StartCoroutine(Close());
        }
    }

    IEnumerator Open()
    {
        FirstAnimator.SetTrigger("Changelight");
        SecondAnimator.SetTrigger("Changelight");
        yield return new WaitForSeconds(0.8f);
        MyLight.enabled = true;
        emissionModule.enabled = true;
        MyLighter.interaction();
        open = true;

    }

    IEnumerator Close()
    {
        emissionModule.enabled = false;
        MyLight.enabled = false;
        FirstAnimator.SetTrigger("Changelight");
        SecondAnimator.SetTrigger("Changelight");
        yield return new WaitForSeconds(0.6f);
        MyLighter.interaction();
        open= false;
    }

    public IEnumerator OutsideClose()
    {
        emissionModule.enabled = false;
        MyLight.enabled = false;

        FirstAnimator.StopPlayback();
        SecondAnimator.StopPlayback();

        yield return new WaitForSeconds(0.05f);

        FirstAnimator.StartPlayback();
        SecondAnimator.StartPlayback();

        MyLighter.interaction();
        open = false;
    }
}
