﻿using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class PlayerStats : MonoBehaviour
{
    [SerializeField]
    private float health;
    [SerializeField]
    private float maxhealth = 100f;
    [SerializeField]
    public Image healthBar;

    [SerializeField]
    private float armor;
    [SerializeField]
    private float maxarmor = 100f;
    [SerializeField]
    public Image armorBar;

    [SerializeField]
    private float hunger;
    [SerializeField]
    private float maxhunger = 100f;
    [SerializeField]
    public Image hungerBar;
    public Image Shield;

    [SerializeField]
    private float thirst;
    [SerializeField]
    private float maxthirst = 100f;
    [SerializeField]
    public Image thirstBar;

    [HideInInspector]
    public float temperature;
    [SerializeField]
    private float maxtemperature = 80f;
    [SerializeField]
    public Image temperatureBar;

    public float timeHungerLost = 0.1f ;
    public float timeThirstLost = 0.07f;
    void Start()
    {
        health = maxhealth;
        hunger = maxhunger;
        thirst= maxthirst;
        temperature = 20;
    }

    void LateUpdate()
    {
        updateHealthBar();
        updateHungerAndThirstBar();
        updatetemperatureBar();
        updateArmorBar();

    }
    void updateHealthBar()
    {
        healthBar.fillAmount = health / maxhealth;  // affichage graphique de la barre de vie
    }

    void updateArmorBar()
    {
        armorBar.fillAmount = armor / maxarmor;
    }
    void updatetemperatureBar()
    {
        temperatureBar.fillAmount = (temperature+20) / maxtemperature;
    }


    void updateHungerAndThirstBar()
    {
        if(hunger - Time.deltaTime * timeHungerLost <=0)
            health -= Time.deltaTime * 1;
        else
            hunger -= Time.deltaTime * timeHungerLost;
            
        if(thirst - Time.deltaTime * timeThirstLost <= 0)
            health -= Time.deltaTime * 1;
        else
            thirst -= Time.deltaTime * timeThirstLost;


        hungerBar.fillAmount = hunger / maxhunger;
        thirstBar.fillAmount = thirst / maxthirst;
    }

    public void ChangeThirst(int value) 
    {
        thirst += value;
        if (thirst < 0)
            thirst = 0; 

        else if (thirst > maxthirst)
            thirst = maxthirst;

    }

    public void ChangeHunger(int value)
    {
        hunger += value;
        if (hunger < 0)
            hunger = 0;

        else if (hunger > maxhunger)
            hunger = maxhunger;

    }

    public void TakeDamage(float damageValue)   // Fonction qui met des dommages à notre personnage
    {
        if (armor > 0)                     // Si il reste du bouclier, mais pas assez pour prendre en charge la totalité des dégâts
        {
            damageValue -= armor;               // On décrémente nos domages en fonction de ce que le bouclier peut absorber
            armor = 0;                          // On enlève la totalité du bouclier
            Shield.enabled = false;             // On masque l'icône du bouclier dans l'UI
            if (health > damageValue)           // Si il reste assez de points de vie
            {
                health -= damageValue;          // Décrémente la vie de notre personnage en fonction des dégâts restant
            }
        }
        else                                    // Si il il n'y a pas de bouclier
        {
            health -= damageValue;              // Décrémente la vie de notre personnage en fonction des dégâts
        }

        if (health <= 0)
        {
            Die();                           // Appelle la fonction de mort du personnage (A CREER)
        }
    }

    public void ChangeArmor(int change) // valeur change negative si on enlève de l'armure, positive dans le cas inverse  
    {
        Shield.enabled = false;               
        armor += change;
        if (armor > 0)
            Shield.enabled = true;                  // On affiche l'icône du bouclier dans l'UI

        if (armor < 0)
            armor = 0; 
        else if (armor > 70)
            armor = 70;
    }

    public void ChangeBrutHealth(int change)
    {
        health += change;
        if (health <= 0)
        {
            health = 0;
            Die();
        }

        else if (health > maxhealth)
            health  = maxhealth;
    }

    void Die()
    {
        // à implémenter
    }
}
