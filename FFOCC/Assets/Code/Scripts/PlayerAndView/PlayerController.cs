﻿using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [Header("Movement")]
    public float speed = 5;

    [Header("Running")]
    public bool m_canRun = true;
    public bool m_IsRunning { get; private set; }
    public float m_runSpeed = 9;
    public KeyCode m_runningKey = KeyCode.LeftShift;

    Rigidbody m_rigidbody;
    /// <summary> Functions to override movement speed. Will use the last added override. </summary>
    public List<System.Func<float>> m_speedOverrides = new List<System.Func<float>>();

    [HideInInspector] public GameObject m_weapon = null;

    // Inventory 

    public bool m_showInventory = false;


    public FirstPersonLook m_Camera;

    public static PlayerController instance; // Singelton

    GameObject m_inventory;
    GameObject m_craftSystem;
    GameObject m_characterSystem;

    void Awake()
    {
        // Get the rigidbody on this.
        m_rigidbody = GetComponent<Rigidbody>();
        if (instance != null && instance != this)
            Destroy(gameObject);    // Suppression d'une instance précédente (sécurité...sécurité...)

        instance = this;
    }
    private void Start()
    {
        m_Camera = FirstPersonLook.instance.GetComponent<FirstPersonLook>();
        PlayerInventory playerInv = GetComponent<PlayerInventory>();
        if (playerInv.m_inventory != null)
            m_inventory = playerInv.m_inventory;
        if (playerInv.m_craftSystem != null)
            m_craftSystem = playerInv.m_craftSystem;
        if (playerInv.m_characterSystem != null)
            m_characterSystem = playerInv.m_characterSystem;
    }

    void FixedUpdate()
    {
        if (!lockMovement())
        {
            // Update IsRunning from input.
            m_IsRunning = m_canRun && Input.GetKey(m_runningKey);

            // Get targetMovingSpeed.
            float targetMovingSpeed = m_IsRunning ? m_runSpeed : speed;
            if (m_speedOverrides.Count > 0)
            {
                targetMovingSpeed = m_speedOverrides[m_speedOverrides.Count - 1]();
            }

            // Get targetVelocity from input.
            Vector2 targetVelocity = new Vector2(Input.GetAxis("Horizontal") * targetMovingSpeed, Input.GetAxis("Vertical") * targetMovingSpeed);

            // Apply movement.
            m_rigidbody.velocity = transform.rotation * new Vector3(targetVelocity.x, m_rigidbody.velocity.y, targetVelocity.y);
            m_Camera.m_lock_camera = false;
            GetComponent<Crouch>().canCrouch = true;
            if (m_weapon != null)
                if (m_weapon.GetComponent<WeaponManager>())
                    m_weapon.GetComponent<WeaponManager>().m_LockFire = false;
                else if (m_weapon.GetComponentInChildren<MeleeWeapon>())
                    m_weapon.GetComponentInChildren<MeleeWeapon>().m_LockAttack = false;
                else if (m_weapon.GetComponentInChildren<Lighter>())
                    m_weapon.GetComponentInChildren<Lighter>().Locktoogle = false;
        }
        else
        {
            m_rigidbody.freezeRotation = true;
            m_Camera.m_lock_camera = true;
            GetComponent<Crouch>().canCrouch = false;
            if (m_weapon != null)
                if (m_weapon.GetComponent<WeaponManager>())
                    m_weapon.GetComponent<WeaponManager>().m_LockFire = true;
                else if (m_weapon.GetComponentInChildren<MeleeWeapon>())
                    m_weapon.GetComponentInChildren<MeleeWeapon>().m_LockAttack = true;
                else if (m_weapon.GetComponentInChildren<Lighter>())
                    m_weapon.GetComponentInChildren<Lighter>().Locktoogle = true;
        }
    }
    bool lockMovement()
    {
        if (m_inventory != null && m_inventory.activeSelf)
            return true;
        else if (m_characterSystem != null && m_characterSystem.activeSelf)
            return true;
        else if (m_craftSystem != null && m_craftSystem.activeSelf)
            return true;
        else if (PauseMenu.gameIsPaused)
            return true;
        else
            return false;
    }
}