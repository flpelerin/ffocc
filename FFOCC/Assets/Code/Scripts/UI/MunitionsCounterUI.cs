using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;

public class MunitionsCounterUI : MonoBehaviour
{
    public TMP_Text m_Munitions;

    public GameObject cameraManager;
    public WeaponManager weaponManager;

    //public TMP_Text m_totalMunitionsInInventory; Il faudra ajouter cela lors du merge, pour compter le nombre de munitions de ce type dans l'inventaire au total

    // Update is called once per frame
    void FixedUpdate()
    {
        if (weaponManager = cameraManager.GetComponentInChildren<WeaponManager>())
        {
            m_Munitions.text = weaponManager.m_CurrentAmmo.ToString() + " / " + weaponManager.m_MaxAmmo.ToString();
        }
        else
        {
            m_Munitions.text = null;
        }
    }
}
