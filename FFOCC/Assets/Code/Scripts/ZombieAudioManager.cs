using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

//Ce script r�alise le sound design du Zombie 
//Il propoe les bruit de pas, les grognement et les attacks
public class ZombieAudioManager : MonoBehaviour
{
    [Header("Walking")]
    public AudioClip[] footstepsClips; // Tableau de clips de pas
    public AudioSource walkingAudioSource;
    public float minSpeedToPlaySound = 0.1f; // Seuil de vitesse pour jouer le son de pas
    public float maxSpeedForInterval = 5f; // Vitesse maximale pour l'intervalle minimum entre les pas
    public float minStepInterval = 0.5f; // Intervalle minimum entre les pas

    private int currentFootstepIndex = 0; // Index pour alterner entre les sons de pas
    private float nextStepTime; // Temps avant de jouer le prochain son de pas
    private NavMeshAgent navMeshAgent; // R�f�rence au NavMeshAgent



    [Header("Attacking")]
    public AudioClip attackClip;
    public AudioSource attackingAudioSource;

    [Header("Random Sounds")]
    public AudioClip[] randomSounds; // Sons al�atoires
    public float minRandomSoundInterval = 5f; // Intervalle minimum entre les sons al�atoires
    public float maxRandomSoundInterval = 10f; // Intervalle maximum entre les sons al�atoires
    private float nextRandomSoundTime;

    // Start is called before the first frame update
    void Start()
    {
        navMeshAgent = GetComponent<NavMeshAgent>(); // R�cup�re le NavMeshAgent
        nextStepTime = Time.time; // Initialise le temps du prochain pas
        nextRandomSoundTime = Time.time + Random.Range(minRandomSoundInterval, maxRandomSoundInterval); // Initialise le prochain temps pour un son al�atoire
    }

    // Update is called once per frame
    void Update()
    {
        float speed = navMeshAgent.velocity.magnitude;

        if (speed > minSpeedToPlaySound)
        {
            float interval = Mathf.Lerp(minStepInterval, 1f / speed, Mathf.Clamp01(speed / maxSpeedForInterval));

            if (Time.time >= nextStepTime)
            {
                PlayFootstepSound();
                nextStepTime = Time.time + interval; // D�finit le temps du prochain pas
            }
        }
        if (Time.time >= nextRandomSoundTime)
        {
            PlayRandomSound();
            nextRandomSoundTime = Time.time + Random.Range(minRandomSoundInterval, maxRandomSoundInterval);
        }
    }

    void PlayFootstepSound()
    {
        if (footstepsClips.Length == 0 || walkingAudioSource == null)
            return;

        AudioClip clip = footstepsClips[currentFootstepIndex]; // S�lectionne le son de pas en cours

        walkingAudioSource.PlayOneShot(clip);

        currentFootstepIndex = (currentFootstepIndex + 1) % footstepsClips.Length; // Alterne � l'autre son de pas
    }

    public void PlayAttackSound()
    {
        if (attackClip == null || attackingAudioSource == null)
            return;

        attackingAudioSource.PlayOneShot(attackClip);
    }

    void PlayRandomSound()
    {
        if (randomSounds.Length == 0 || walkingAudioSource == null)
            return;

        AudioClip clip = randomSounds[Random.Range(0, randomSounds.Length)];
        walkingAudioSource.PlayOneShot(clip);
    }
}
