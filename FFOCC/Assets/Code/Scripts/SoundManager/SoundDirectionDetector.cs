using UnityEngine;
using TMPro;
using System.Collections.Generic;

public class SoundDirectionDetector : MonoBehaviour
{
    public Transform playerTransform;
    public TextMeshProUGUI soundText;

    private HashSet<string> encounteredSounds = new HashSet<string>();
    public float detectionRadius = 100f; 
    public float updateInterval = 0.5f; 

    void Start()
    {
        if (playerTransform == null)
        {
            Debug.LogError("Le transform du joueur n'est pas d�fini. Assurez-vous d'assigner le transform du joueur.");
            return;
        }

        InvokeRepeating("UpdateSoundDirections", 0f, updateInterval);
    }

    void UpdateSoundDirections()
    {
        string soundTextToShow = "";

        AudioSource[] audioSources = FindObjectsOfType<AudioSource>();
        foreach (AudioSource audioSource in audioSources)
        {
            if (audioSource.isPlaying)
            {
                float distanceToPlayer = Vector3.Distance(audioSource.transform.position, playerTransform.position);
                if (distanceToPlayer <= detectionRadius)
                {
                    Vector3 soundDirection = (audioSource.transform.position - playerTransform.position).normalized;
                    float angle = Vector3.SignedAngle(playerTransform.forward, soundDirection, Vector3.up);

                    if (audioSource.clip && !encounteredSounds.Contains(audioSource.clip.name))
                    {
                        encounteredSounds.Add(audioSource.clip.name);

                        if (angle < 0)
                            soundTextToShow += string.Format("< {0}\n", audioSource.clip.name);
                        else
                            soundTextToShow += string.Format("{0}  >\n", audioSource.clip.name);
                    }
                    else if (audioSource.gameObject.GetComponent<ZombieAudioManager>())
                    {
                        if (angle < 0)
                            soundTextToShow += string.Format("< {0}\n", "Zombie");
                        else
                            soundTextToShow += string.Format("{0}  >\n", "Zombie");
                    }
                }
            }
        }

        if (soundText != null)
            soundText.text = soundTextToShow;

        encounteredSounds.Clear();
    }
}
