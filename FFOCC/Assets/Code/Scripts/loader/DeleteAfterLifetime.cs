using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Lifetime {
    public float min, max,
                 current;

    public Lifetime(float min, float max) {
        this.min = min;
        this.max = max;
    }

    public void ResetLifetime() { this.current = Random.Range(min, max); }
}


public class DeleteAfterLifetime : MonoBehaviour
{
    public GameObject targetObject;
    public Lifetime lifetime = new Lifetime(10, 20);

    public float updateDelay = 1f;

    public bool pauseOnTrigger = false;
    public int numTriggerEnters = 0;

    public string[] triggerTags = { "Player", "MainCamera" };


    void UpdateLifetime() {
        if (pauseOnTrigger && numTriggerEnters > 0) return;
        
        lifetime.current -= updateDelay;

        if (lifetime.current <= 0) {
            Destroy(targetObject);
            Destroy(gameObject);
        }
    }

    bool HasTriggerTag(GameObject obj) {
        foreach (string tag in triggerTags) {
            if (obj.CompareTag(tag)) return true;
        }
        return false;
    }

    void OnLoaderEnter(GameObject obj) { if(pauseOnTrigger && HasTriggerTag(obj)) numTriggerEnters++; }
    void OnLoaderExit(GameObject obj) { if(pauseOnTrigger && HasTriggerTag(obj)) numTriggerEnters--; }

    void Awake () { 
        lifetime.ResetLifetime();

        targetObject = targetObject == null
                ? transform.parent != null 
                    ? transform.parent.gameObject 
                    : gameObject
                : targetObject;

        if (!GetComponent<SphereAreaTrigger>()) gameObject.AddComponent<SphereAreaTrigger>();

        if (GetComponent<LocalLoader>())
            Destroy(GetComponent<LocalLoader>());

        InvokeRepeating ("UpdateLifetime", 0, updateDelay);
    }
}
