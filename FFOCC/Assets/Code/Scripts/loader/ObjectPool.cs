using System.Collections;
using System.Collections.Generic;
using UnityEngine;




public class ObjectPool : MonoBehaviour
{

    public GameObject[] prefabs;
    public GameObject localLoader = null;
    
    public float SpawnDelay = .5f;
    public int maxInstanceCount = 100;
    private int currentInstanceCount = 0;

    public List<GameObject> instances;


    GameObject GetRandomObject() {
        if (prefabs.Length == 0) return null;

        int randomIndex = Random.Range(0, prefabs.Length);
        return prefabs[randomIndex];
    }


    Vector3 GetRandomPosition() {
        Vector3 pos = transform.position;
        Vector3 scale = transform.localScale;

        float x = Random.Range(-scale.x / 2, scale.x / 2);
        float y = 1;
        float z = Random.Range(-scale.z / 2, scale.z / 2);

        Vector3 offset = new Vector3(x, y, z);
        offset = transform.rotation * offset; // Rotate the offset by the spawner's rotation

        return pos + offset;
    }

    // Function that gets a random rotation, given a boolean tuple for all 3 axis (false means freeze the axis)
    Quaternion GetRandomRotation(bool x, bool y, bool z) { return Quaternion.Euler(x ? Random.Range(0, 360) : transform.rotation.x, y ? Random.Range(0, 360) : transform.rotation.y, z ? Random.Range(0, 360) : transform.rotation.z); }


    public void UpdateInstanceCount() {
        for (int i = 0; i < instances.Count; i++) {
            if (instances[i] == null) {
                instances.RemoveAt(i);
                currentInstanceCount--;
            }
        }
    }

    IEnumerator SpawnObject() {
        UpdateInstanceCount();
        if (currentInstanceCount >= maxInstanceCount) goto end;

        GameObject obj = GetRandomObject();
        if (obj == null) goto end;

        GameObject inst = Instantiate(obj, GetRandomPosition(), GetRandomRotation(false, true, false));
        instances.Add(inst);
        currentInstanceCount++;

end:
        yield return new WaitForSeconds(SpawnDelay);
        StartCoroutine(SpawnObject());
    }


    void OnEnable() {
        

        StartCoroutine(SpawnObject());
    }

    void Start() {
        
        localLoader = Instantiate(localLoader, transform.position, Quaternion.identity, transform.parent);
        localLoader.AddComponent<RepositionerDownUp>();
        localLoader.SendMessage("SetLoadableObject", gameObject, SendMessageOptions.DontRequireReceiver);
        localLoader.transform.localScale = transform.localScale / 2;
        localLoader.name = name + " localLoader";

        gameObject.SetActive(false);
    }

}