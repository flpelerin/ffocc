using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;

public class SphereAreaTrigger : MonoBehaviour
{
    public float updateDelay = -1f;      // negative value disables update
    private float radius;                // automatically set to the largest scale value

    List<GameObject> currentObjects = new List<GameObject>();


    float VecMax(Vector3 vec) { return Mathf.Max(vec.x, vec.y, vec.z); }


    void SphereEnter(GameObject obj) {
        currentObjects.Add(obj);
        gameObject.SendMessage("OnSphereAreaEnter", obj, SendMessageOptions.DontRequireReceiver);
    }

    void SphereStay(GameObject obj) {
        gameObject.SendMessage("OnSphereAreaStay", obj, SendMessageOptions.DontRequireReceiver);
    }

    void SphereExit(GameObject obj) {
        currentObjects.Remove(obj);
        gameObject.SendMessage("OnSphereAreaExit", obj, SendMessageOptions.DontRequireReceiver);
    }


    List<GameObject> SimpleSphereOverlap(Vector3 pos, float radius) {
        /* Detects all collision between pos, radius and all objects in the scene
           Doesn't need colliders on object. Simply uses GameObject's position and radius to simulate sphere overlapping.
           Iterates through all the objects in the scene and returns an list of overlaping GameObjects */

        List<GameObject> hitObjects = new List<GameObject>();
        SphereAreaTrigger[] allTriggers = FindObjectsOfType<SphereAreaTrigger>();
        GameObject[] allObjects = new GameObject[allTriggers.Length];
        Vector3[] allPositions = new Vector3[allTriggers.Length];
        Vector3[] allScales = new Vector3[allTriggers.Length];
    
        GameObject self = gameObject;
        Thread[] threads = new Thread[System.Environment.ProcessorCount];

        for (int i = 0; i < allTriggers.Length; i++) {
            allObjects[i] = allTriggers[i].gameObject;
            allPositions[i] = allTriggers[i].transform.position;
            allScales[i] = allTriggers[i].transform.localScale;
        }


        // Divide the work among the threads
        for (int i = 0; i < threads.Length; i++) {
            int startIndex = i * (allTriggers.Length / threads.Length);
            int endIndex = (i + 1) * (allTriggers.Length / threads.Length);

            threads[i] = new Thread(() => {
                for (int j = startIndex; j < endIndex; j++) {
                    GameObject obj = allObjects[j];
                    SphereAreaTrigger trigger = allTriggers[j];
                    Vector3 objPos = allPositions[j];
                    Vector3 objScale = allScales[j];

                    if (obj == self || trigger == null) continue;

                    if (Vector3.Distance(pos, objPos) < radius + VecMax(objScale))
                        lock (hitObjects)
                            hitObjects.Add(obj);
                }
            });

            threads[i].Start();
        }

        foreach (Thread thread in threads)
            thread.Join();

        return hitObjects;
    }

    public float GetRadius() { return radius; }


    IEnumerator CheckCollisions() {
        /*  A collision is defined by the intersection of two spheres.
            A sphere is defined in self space by a center point and a radius, given by its scale.
            A second sphere is defined by the center point and radius for each object in the scene.

            This should work similarly to Physics.OverlapSphere, but without the need of colliders on any of the objects
        */

        List<GameObject> hitObjects = SimpleSphereOverlap(transform.position, radius);
        List<GameObject> exitedObjects = new List<GameObject>();

        foreach (var obj in hitObjects) {
            if (currentObjects.Contains(obj)) {
                SphereStay(obj);
                continue;
            }

            SphereEnter(obj);
        }

        foreach (var obj in new List<GameObject>(currentObjects)) {
            if (exitedObjects.Contains(obj)) continue;

            if (!hitObjects.Contains(obj)) {
                SphereExit(obj);
                exitedObjects.Add(obj);
            }
        }

        yield return new WaitForSeconds(updateDelay);
        StartCoroutine(CheckCollisions());
    }


    void Awake() {
        radius = VecMax(transform.localScale);

        if (updateDelay >= 0)
            StartCoroutine(CheckCollisions());
    }
}
