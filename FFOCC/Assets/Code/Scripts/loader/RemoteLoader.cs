using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoteLoader : MonoBehaviour
{
    private GameObject origin;

    public void OnSphereAreaEnter(GameObject obj) {
        if (obj == null) return;
        obj.SendMessage("OnLoaderEnter", origin, SendMessageOptions.DontRequireReceiver);
    }

    public void OnSphereAreaExit(GameObject obj) { 
        if (obj == null) return;
        obj.SendMessage("OnLoaderExit", origin, SendMessageOptions.DontRequireReceiver);
    }

    void Awake () { origin = transform.parent != null ? transform.parent.gameObject : gameObject; }
}
