using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class RepositionerDownUp : MonoBehaviour
{

    public bool preventCollision = true;


    RaycastHit? GetFirstNonSelfHit(Vector3 pos, Vector3 dir) {
        RaycastHit[] hits = Physics.RaycastAll(pos, dir, Mathf.Infinity);

        foreach (var hit in hits)
            if (hit.collider.gameObject != gameObject)
                return (hit.collider.gameObject.GetComponent<RepositionerDownUp>() != null && preventCollision == true) ? null : hit;

        return null;
    }


    // Function that raycasts down and up from self's position, returning the first hit point
    RaycastHit? RaycastDownUp(Vector3 pos) {
        return GetFirstNonSelfHit(pos, transform.up) ?? GetFirstNonSelfHit(pos, -transform.up);
    }


    Vector3? GetQuadPosition(RaycastHit? hitPoint) { return (hitPoint != null) ? ((RaycastHit)hitPoint).point : null; }
    Quaternion? GetQuadRotation(RaycastHit? hitPoint) { return (hitPoint != null) ? Quaternion.FromToRotation(Vector3.up, ((RaycastHit)hitPoint).normal) : null; }


    public void MoveToNearestSurface() {
        RaycastHit? hit = RaycastDownUp(transform.position);
        Vector3? quadPosition = GetQuadPosition(hit);
        Quaternion? quadRotation = GetQuadRotation(hit);


        if (quadPosition != null && quadPosition != Vector3.zero)
            transform.position = (Vector3)quadPosition; //+ new Vector3(0, transform.localScale.y / 2, 0);

        if (quadRotation != null)
            transform.rotation = (Quaternion)quadRotation * Quaternion.Euler(0, transform.rotation.eulerAngles.y, 0);
    }


    void Start() { MoveToNearestSurface(); }
}
