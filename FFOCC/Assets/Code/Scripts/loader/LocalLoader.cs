using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocalLoader : MonoBehaviour
{

    public bool inverseLoader = false;
    public GameObject loadableObject = null;
    public string[] loaderTags = { "Player", "MainCamera" };
    
    private int numEnters = 0;


    bool HasLoaderTag(GameObject obj) {
        foreach (string tag in loaderTags) {
            if (obj.CompareTag(tag)) return true;
        }
        return false;
    }

    void LoadUnloadObject(bool state) {
        if (loadableObject == null) return;
        loadableObject.SetActive(state);
    }


    public void OnLoaderEnter(GameObject obj) {
        if (!HasLoaderTag(obj)) return;
        ++numEnters;

        LoadUnloadObject(!inverseLoader);
    }

    public void OnLoaderExit(GameObject obj) {
        if (!HasLoaderTag(obj)) return;
        if (--numEnters > 0) return;

        LoadUnloadObject(inverseLoader);
    }


    public void SetLoadableObject(GameObject obj) {
        if (obj == null || obj == gameObject) return;
        loadableObject = obj;

        LoadUnloadObject(inverseLoader);
    }

    public void RemoveLoadableObject(GameObject obj) {
        if (obj == null || obj == gameObject) return;
        loadableObject = null;

        LoadUnloadObject(!inverseLoader);
    }


    void Awake() {
        if (loadableObject == gameObject)
            loadableObject = null;

        foreach (Transform child in transform) {
            child.parent = loadableObject.transform;
        }

        LoadUnloadObject(inverseLoader);
    }

}
