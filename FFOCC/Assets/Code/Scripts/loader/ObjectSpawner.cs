using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class ObjectSpawner : MonoBehaviour
{
    public GameObject[] obj;
    public int spawnQuantity = 10;      // -1 means spawn an infinite number of objects
    public float SpawnDelay = 0.5f;
    
    public GameObject localLoader = null;
    private GameObject spawnOrigin = null;

    GameObject GetRandomObject() {
        if (obj.Length == 0) return null;

        int randomIndex = Random.Range(0, obj.Length);
        return obj[randomIndex];
    }


Vector3 GetRandomPosition() {
    Vector3 pos = transform.position;
    Vector3 scale = transform.localScale;

    float x = Random.Range(-scale.x / 2, scale.x / 2);
    float y = 1;
    float z = Random.Range(-scale.z / 2, scale.z / 2);

    Vector3 offset = new Vector3(x, y, z);
    offset = transform.rotation * offset; // Rotate the offset by the spawner's rotation

    return pos + offset;
}

    // Function that gets a random rotation, given a boolean tuple for all 3 axis (false means freeze the axis)
    Quaternion GetRandomRotation(bool x, bool y, bool z) { return Quaternion.Euler(x ? Random.Range(0, 360) : transform.rotation.x, y ? Random.Range(0, 360) : transform.rotation.y, z ? Random.Range(0, 360) : transform.rotation.z); }

    float VecMax(Vector3 vec) { return Mathf.Max(vec.x, vec.y, vec.z); }

    IEnumerator SpawnObjects() {
        List<GameObject> instances = new List<GameObject>();
        
        for (int i = 0; i == -1 || i < spawnQuantity; i++) {

            GameObject obj = GetRandomObject();
            if (obj == null) continue;

            GameObject inst = Instantiate(obj, GetRandomPosition(), GetRandomRotation(false, true, false));
            instances.Add(inst);
            inst.transform.parent = spawnOrigin.transform;

            yield return new WaitForSeconds(SpawnDelay);
        }

exit:   Destroy(gameObject);
    }

    void Start () {
        spawnOrigin = new GameObject();
        spawnOrigin.transform.position = transform.position;
        spawnOrigin.transform.parent = transform.parent;
        spawnOrigin.name = name + " spawnOrigin";

        localLoader = Instantiate(localLoader, spawnOrigin.transform.position, Quaternion.identity, spawnOrigin.transform.parent);
        localLoader.AddComponent<RepositionerDownUp>();
        localLoader.SendMessage("SetLoadableObject", spawnOrigin, SendMessageOptions.DontRequireReceiver);
        localLoader.transform.localScale = transform.localScale / 2;
        localLoader.name = name + " localLoader";

        StartCoroutine(SpawnObjects());
    }
}
