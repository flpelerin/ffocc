using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawSphereGizmo : MonoBehaviour
{
    public Color color = Color.red;
    public bool hideOnPlay = false;

    float vecMax(Vector3 vec) { return Mathf.Max(vec.x, vec.y, vec.z); }

    void OnDrawGizmos() {
        if (hideOnPlay && Application.isPlaying)
            return;

        Gizmos.color = color;
        Gizmos.DrawWireSphere(transform.position, vecMax(transform.localScale));
    }
}
