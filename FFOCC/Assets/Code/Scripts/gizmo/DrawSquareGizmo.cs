using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawSquareGizmo : MonoBehaviour
{
    public Color color = Color.red;
    public bool hideOnPlay = false;

    void OnDrawGizmos() {
        if (hideOnPlay && Application.isPlaying)
            return;

        Gizmos.color = color;
        Matrix4x4 rotationMatrix = Matrix4x4.TRS(transform.position, transform.rotation, transform.localScale);
        Gizmos.matrix = rotationMatrix;
        Gizmos.DrawWireCube(Vector3.zero, Vector3.one);
    }
}
