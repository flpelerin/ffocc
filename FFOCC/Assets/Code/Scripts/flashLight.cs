using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class flashLight : MonoBehaviour
{
    private LightEmetorMimic MyFlashLight;
    private Light MyLight;

    bool LockSwitch = false;

    float timesinceswitch = 0f ;
    [SerializeField] float switchcd = 1f;

    private void Start()
    {
        MyFlashLight = GetComponentInChildren<LightEmetorMimic>();
        MyLight = GetComponentInChildren<Light>();
    }


    void FixedUpdate()
    {
        timesinceswitch += Time.deltaTime;
        if (LockSwitch)
            return;
        if (Input.GetMouseButton(0) && timesinceswitch > switchcd)
        {
            timesinceswitch = 0f;
            MyFlashLight.interaction();
            MyLight.enabled = !MyLight.enabled;
        }
    }

}
