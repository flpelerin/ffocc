using MimicSpace;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.GlobalIllumination;

public class LightEmetorMimic : MonoBehaviour
{
    public bool IsLight;
    public bool interact;
    Movement mimic;
    void Start()
    {
        mimic= FindFirstObjectByType<Movement>();
    }

    // Update is called once per frame
    void LateUpdate()
    {
        // destin� a disparaitre et �tre mis dans interact, laiss� que pour les test. 
        if (interact)
        {
            mimic = FindFirstObjectByType<Movement>();
            interact = false;
            IsLight = !IsLight;
            if (mimic !=null) 
            {             
                mimic.TrouverObjetLePlusProche();
            }

        }
    }

    public void OnMimicSpawned()
    {
        mimic = FindFirstObjectByType<Movement>();
    }
    public void interaction()
    {
        interact = true;
    }

}
