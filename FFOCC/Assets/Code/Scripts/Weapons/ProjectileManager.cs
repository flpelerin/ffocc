using UnityEngine;

public class ProjectileManager : MonoBehaviour
{
    static string[] attackableTags = { "Zombie", "Player"}; // Ajoutez cette variable pour spécifier les tags attaquables

    private void OnParticleCollision(GameObject other)
    {
        if (IsAttackable(other.tag))
        {
            ParticleSystem projectileParticleSystem = GetComponent<ParticleSystem>();
            int i_closest = 0;
            float smallest_Dist = float.MaxValue;

            if (projectileParticleSystem != null)
            {
                ParticleSystem.Particle[] particles = new ParticleSystem.Particle[projectileParticleSystem.main.maxParticles];
                int numParticlesAlive = projectileParticleSystem.GetParticles(particles);

                for (int i = 0; i < numParticlesAlive; i++)
                {
                    float dist = Vector3.Distance(particles[i].position, other.transform.position);

                    if (dist < smallest_Dist)
                    {
                        i_closest = i;
                        smallest_Dist = dist;
                    }
                }

                Debug.Log("Hit: " + other.gameObject);

                if(other.tag == "Zombie")
                {
                    other.gameObject.GetComponent<ZombieAI>().TakeDamage(transform.parent.GetComponent<WeaponManager>().m_damage);
                }

                particles[i_closest].remainingLifetime = 0f;
                projectileParticleSystem.SetParticles(particles, numParticlesAlive);
            }
        }
    }

    static public bool IsAttackable(string tag)
    {
        foreach (string attackableTag in attackableTags)
        {
            if (tag == attackableTag)
            {
                return true;
            }
        }

        return false;
    }
}
