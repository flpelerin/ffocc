using UnityEngine;
using System.Collections.Generic;

public class MeleeWeapon : MonoBehaviour
{
    [SerializeField] float attackCooldown = 1f;
    public int m_damage;

    bool onAttackCooldown;
    bool Attacking = false;
    bool hastouched;

    // Nouveau bool�en pour sp�cifier si l'arme peut toucher plusieurs ennemis
    [SerializeField] bool canHitMultipleEnemies = false;

    Collider m_attackCollider;
    [HideInInspector] public bool m_LockAttack = false;

    Animator m_animator;
    [SerializeField] AudioSource m_AttackSound;
    float m_TimeSinceAttack = 0f;

    float currentAnimDuration;

    // Nouvelle liste pour stocker les ennemis touch�s
    List<Collider> enemiesHit = new List<Collider>();

    void Start()
    {
        m_animator = GetComponentInChildren<Animator>();
        m_attackCollider = GetComponentInChildren<Collider>();
        m_AttackSound = GetComponentInChildren<AudioSource>();

        m_attackCollider.enabled = false;

        // Appelle la fonction pour ajuster le cooldown en fonction de la dur�e des animations
        AdjustCooldownBasedOnAnimation();
    }

    // Fonction pour ajuster le cooldown en fonction de la dur�e des animations
    void AdjustCooldownBasedOnAnimation()
    {
        float longestAnimationDuration = 0f;

        // Parcourt toutes les animations et trouve la plus longue
        foreach (AnimationClip clip in m_animator.runtimeAnimatorController.animationClips)
        {
            if (clip.length > longestAnimationDuration)
            {
                longestAnimationDuration = clip.length;
            }
        }

        // Ajoute un d�lai suppl�mentaire de 0.5f pour �tre s�r
        float adjustedCooldown = longestAnimationDuration + 0.5f;

        // V�rifie si le cooldown initial est inf�rieur � l'ajustement, sinon, utilise le cooldown initial
        attackCooldown = Mathf.Max(attackCooldown, adjustedCooldown);
    }

    void FixedUpdate()
    {
        m_TimeSinceAttack += Time.deltaTime;

        if (m_TimeSinceAttack > attackCooldown)
        {
            onAttackCooldown = false;
            m_LockAttack = false;
            // R�initialise la liste des ennemis touch�s � chaque attaque
            enemiesHit.Clear();
        }

        if (m_TimeSinceAttack > currentAnimDuration)
            m_attackCollider.enabled = false;

        if (m_TimeSinceAttack > attackCooldown)
        {
            Attacking = false;
        }
    }


    void Update()
    {
        if (Input.GetMouseButton(0) && !onAttackCooldown && !Attacking && !m_LockAttack)
        {
            m_TimeSinceAttack = 0f;
            hastouched = false;
            Attacking = true;
            onAttackCooldown = true;


            int randomAttackIndex = Random.Range(1, m_animator.runtimeAnimatorController.animationClips.Length+1);
            currentAnimDuration = m_animator.runtimeAnimatorController.animationClips[randomAttackIndex-1].length;
            string attackTrigger = "Attack" + randomAttackIndex;

            m_animator.SetTrigger(attackTrigger);
            m_AttackSound.volume = 100f;
            m_AttackSound.Play();
            m_attackCollider.enabled = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Successful attack on: " + other.gameObject.name);

        if (!ProjectileManager.IsAttackable(other.tag) || (hastouched && !canHitMultipleEnemies))
            return;

        enemiesHit.Add(other);

        if (other.tag == "Zombie")
        {
            other.gameObject.GetComponent<ZombieAI>().TakeDamage(m_damage);
        }

        hastouched = true;
    }
}
