using UnityEngine;
using System.Collections.Generic;

public class WeaponManager : MonoBehaviour
{
    [Header("Weapon Miscs")]
    [SerializeField] float m_Ammospeed = 20;
    [HideInInspector] public int m_CurrentAmmo = 20;
    public int m_MaxAmmo = 20;

    [SerializeField] float m_gunaccuracy = 100; // Permet de modifier la pr�cision de l'arme
    public int m_damage = 5;

    [SerializeField] float m_ReloadingCooldown = 1f; // Temps pour reload 
    public bool m_IsReloading = false;

    [SerializeField] float m_FiringCooldown = 0.5f; // Temps n�cessaire entre chaque tir
    bool m_onFiringCooldown = false;
    float m_TimeSinceShot = 3; // Base value to not make it smoke when taking da gun

    [SerializeField] AmmoType _ammotype; // Munitions utilis�es 

    public bool m_LockFire = false;
    [SerializeField] int m_NbAmmoFired = 1;



    [Header("Particles GameObjects")]
    [SerializeField] ParticleSystem m_Shootparticles = null;  // balles tir�es 
    [SerializeField] ParticleSystem m_Shardsparticles = null; // flamme en bout d'arme 
    [SerializeField] ParticleSystem m_Shellparticles = null;  // douilles de l'arme 
    [SerializeField] ParticleSystem m_Smokeparticles = null;  // fum�e du canon 

    private AudioPoolManager m_SoundManager;
    [Header("Sound System")]

    [SerializeField] AudioClip m_ShootingSound;
    [SerializeField] AudioClip m_ReloadingSound;
    [SerializeField] AudioClip m_EmptySound;


    static PlayerInventory _inventory; // inventaire du joueur


    private void Start()
    {
        m_SoundManager= AudioPoolManager.instance;
        var partMain = m_Shootparticles.main; // r�cup�re le main de la particule 
        partMain.startSpeed = m_Ammospeed; // on assigne la vitesse de la balle � la particule

        if (_inventory == null) // si l'inventory n'a pas d�j� �t� r�cup�r�, on le r�cup�re
            _inventory = PlayerController.instance.GetComponent<PlayerInventory>();
    }

    private void Update()
    {
        if (Input.GetMouseButton(0) && !m_onFiringCooldown && !m_IsReloading)
        {
            FireWeapon();
        }
        if (Input.GetKeyDown(KeyCode.R) && !m_IsReloading)
        {
            m_IsReloading = true;
        }

    }

    void FixedUpdate()
    {
        m_TimeSinceShot += Time.deltaTime;

        if (m_TimeSinceShot > m_FiringCooldown) // on permet au joueur de tirer si le firing cooldown est pass�
        {
            m_onFiringCooldown = false;
        }
        if (m_TimeSinceShot >= 2 && m_TimeSinceShot <= 3) // Si le joueur n'a pas tirer depuis 2s, on fait de la fum�e pendant 1s
        {
            m_Smokeparticles.Emit(1);
        }

        if (m_TimeSinceShot > m_ReloadingCooldown && m_IsReloading)
        {
            Reload();
        }
    }

    private void Reload()
    {
        m_IsReloading = false;
        List<Item> ammos = _inventory.GetAmmos();  // on r�cup�re toutes les ammos pr�sentes dans l'inventaire
        if (ammos.Count == 0)
        {
            return;
        }

        m_SoundManager.PlaySound(m_ReloadingSound);
        List<Item> CorrectAmmos = new List<Item>(); // list des ammos correspondantes � l'arme 

        int cpt = 0; // nombre de munitions trouv�es 

        foreach (Item item in ammos) // test chaque munitions r�cup�r�es et regarde si elles correspondent � celle de l'arme actuel
        {
            if (item.itemValue <= 0) // si la valeur de l'item est inf�rieure ou �gale � 0 (si on en a plus) 
                _inventory.deleteItemFromInventoryWithGameObject(item);

            if (cpt >= m_MaxAmmo) // 
                break;

            if (_ammotype == AmmoType.Nine_mm && item.itemAttributes[0].attributeName == "9mm")
            {
                if (item.itemValue + m_CurrentAmmo > m_MaxAmmo) // Si cet item seul permet de r�cup�rer plus de munition
                {
                    item.itemValue -= m_MaxAmmo - m_CurrentAmmo;
                    m_CurrentAmmo = m_MaxAmmo;
                   
                    return;
                }
                else
                {
                    cpt += item.itemValue;
                    CorrectAmmos.Add(item);
                }
            }

            if (_ammotype == AmmoType.PointQuaranteCinq && item.itemAttributes[0].attributeName == ".45")
            {
                if (item.itemValue + m_CurrentAmmo > m_MaxAmmo)
                {
                    item.itemValue -= m_MaxAmmo - m_CurrentAmmo;
                    m_CurrentAmmo = m_MaxAmmo;

                    return;
                }
                else
                {
                    cpt += item.itemValue;
                    CorrectAmmos.Add(item);
                }
            }

            if (_ammotype == AmmoType.CinqCinquanteSixM4 && item.itemAttributes[0].attributeName == "5,56-M4")
            {
                if (item.itemValue + m_CurrentAmmo > m_MaxAmmo)
                {
                    item.itemValue -= (m_MaxAmmo - m_CurrentAmmo);
                    m_CurrentAmmo = m_MaxAmmo;
                    
                    return;
                }
                else
                {
                    cpt += item.itemValue;
                    CorrectAmmos.Add(item);
                }
            }

            if (_ammotype == AmmoType.PointCinquant && item.itemAttributes[0].attributeName == ".50")
            {
                if (item.itemValue + m_CurrentAmmo > m_MaxAmmo)
                {
                    item.itemValue -= m_MaxAmmo - m_CurrentAmmo;
                    m_CurrentAmmo = m_MaxAmmo;
                    return;
                }
                else
                {
                    cpt += item.itemValue;
                    CorrectAmmos.Add(item);
                }
            }

            if (_ammotype == AmmoType.Calibre12 && item.itemAttributes[0].attributeName == "Calibre 12")
            {
                if (item.itemValue + m_CurrentAmmo > m_MaxAmmo)
                {
                    item.itemValue -= m_MaxAmmo - m_CurrentAmmo;
                    m_CurrentAmmo = m_MaxAmmo;
                    return;
                }
                else
                {
                    cpt += item.itemValue;
                    CorrectAmmos.Add(item);
                }
            }

            if (_ammotype == AmmoType.CinqQuaranteCinq && item.itemAttributes[0].attributeName == "5,45")
            {
                if (item.itemValue + m_CurrentAmmo > m_MaxAmmo)
                {
                    item.itemValue -= m_MaxAmmo - m_CurrentAmmo;
                    m_CurrentAmmo = m_MaxAmmo;
            
                    return;
                }
                else
                {
                    cpt += item.itemValue;
                    CorrectAmmos.Add(item);
                }
            }

            if (_ammotype == AmmoType.CinqCinquanteSixM249 && item.itemAttributes[0].attributeName == "5,56-M249")
            {
                if (item.itemValue + m_CurrentAmmo > m_MaxAmmo)
                {
                    item.itemValue -= m_MaxAmmo - m_CurrentAmmo;
                    m_CurrentAmmo = m_MaxAmmo;
                    return;
                }
                else
                {
                    cpt += item.itemValue;
                    CorrectAmmos.Add(item);
                }
            }


        }
        if (cpt + m_CurrentAmmo >= m_MaxAmmo)
        {
            int ammo_removed = m_CurrentAmmo;
            foreach (Item item in CorrectAmmos)
            {
                if (ammo_removed < m_MaxAmmo)
                {
                    if (ammo_removed + item.itemValue < m_MaxAmmo)
                    {
                        ammo_removed += item.itemValue;
                        item.itemValue -= item.itemValue;
                        _inventory.deleteItemFromInventoryWithGameObject(item);
                    }
                    else
                    {
                        item.itemValue -= m_MaxAmmo - ammo_removed;
                        ammo_removed += m_MaxAmmo - ammo_removed;
                        if (item.itemValue <= 0)
                        {
                            _inventory.deleteItemFromInventoryWithGameObject(item);
                        }
                    }

                }
            }
            m_CurrentAmmo = ammo_removed;
        }
        else
        {
            foreach (var item in CorrectAmmos)
            {  
                item.itemValue -= item.itemValue;
                _inventory.deleteItemFromInventoryWithGameObject(item);
            }
            m_CurrentAmmo = cpt + m_CurrentAmmo;

        }
    }

    void FireWeapon()
    {
        if (!m_LockFire)
        {
            if (m_CurrentAmmo > 0)
            {
                m_onFiringCooldown = true;
                m_SoundManager.PlaySound(m_ShootingSound);
                m_TimeSinceShot = 0;
                m_CurrentAmmo--;
                m_Shootparticles.Emit(m_NbAmmoFired);
                m_Shardsparticles.Emit(1);
                m_Shellparticles.Emit(1);
                var partShape = m_Shootparticles.shape;
                partShape.angle = (100 - m_gunaccuracy);

                var partMain = m_Shootparticles.main;
                partMain.startSpeed = m_Ammospeed;
            }
            else
            {
                // son de tir à vide 
                m_SoundManager.PlaySound(m_EmptySound);
            }
        }
        else
        {
            Debug.Log("Le tir est verrouill�");
        }
    }

    enum AmmoType
    {
        Nine_mm,
        PointQuaranteCinq,
        CinqCinquanteSixM4,
        PointCinquant,
        Calibre12,
        CinqQuaranteCinq,
        CinqCinquanteSixM249
    }

}

