using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class healTake : MonoBehaviour
{
    public float healthValue;
    public PlayerStats player;
    public float healDistance;

    private void Awake()
    {
        player = PlayerStats.FindObjectOfType<PlayerStats>();
    }
    // Update is called once per frame
    void Update()
    {
        if (Vector3.Distance(player.transform.position, gameObject.transform.position) < healDistance)
        {
            //player.takeHealth(healthValue);
            Destroy(gameObject);
        }
    }
}
